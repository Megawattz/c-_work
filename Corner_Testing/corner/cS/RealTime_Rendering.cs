﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace corner
{ 
    class RealTime_Render : INotifyPropertyChanged
    {
        #region var
        public List<Ellipse> ellipses;
        public List<Polyline> Polylines;
        private Ellipse ellipse;
        private DispatcherTimer timer;
        private Canvas Holder;
        private Ellipse Selected_elipse;
        public Brush fill
        {
            get
            {
                return _brush;
            }
            set
            {
                if (_brush != value)
                {
                    _brush = value;
                    onPropertyChanged();
                }
            }
        }
        private Brush  _brush;
        public bool stopper;

        public RealTime_Render DataContext { get; }

        Polyline _Line;

        private double _X;
        public double X
        {
            get { return _X; }
            set
            {
                if (_X != value)
                {
                    _X = value;
                    onPropertyChanged();
                }
            }
        }

        private double _Y;
        public double Y
        {
            get { return _Y; }
            set
            {
                if (_Y != value)
                {
                    _Y = value;
                    onPropertyChanged();
                }
            }
        }

        private int _Line_Size;
        public int Line_Size
        {
            get { return _Line_Size; }
            set
            {
                if (value != _Line_Size)
                {
                    _Line_Size = value;
                    onPropertyChanged();
                }
            }

        }

        #endregion
        #region property changed event 
        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged([CallerMemberName] string property_name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property_name));
        }
        #endregion
        #region constuctor
        public RealTime_Render(ref Canvas _Holder)
        {
            DataContext = this;
            Line_Size = 5;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1);
            Holder = _Holder;
            X = 0;
            Y = 0;
            stopper = false;
            fill = new SolidColorBrush(Colors.Yellow);
            ellipses = new List<Ellipse>();
            Polylines = new List<Polyline>();
        }
        #endregion
        public void start_timer(object sender)
        {
            timer.Start();
            if (sender is Canvas)
            {
                fill = new SolidColorBrush(Colors.Green);
                timer.Tick += Create_Elipse;
            }
        }
        private void Create_Elipse(object sender, EventArgs e)
        {
            if (stopper)
            {
                ellipse = CreateE(Brushes.Red);
                Point point = Mouse.GetPosition(Holder);
                relocate();
                Holder.Children.Clear();
                Draw_PolyLine();
                Holder.Children.Add(ellipse);
                Selected_elipse = ellipse;
                ellipses.Add(Selected_elipse);

            }
            else
            {
                end();
            }
        }
        /// <summary>
        /// creates an elipse 
        /// </summary>
        /// <param name="brush"></param>
        /// <returns></returns>
        private Ellipse CreateE(System.Windows.Media.Brush brush)
        {
            remove_Elipse();
            Ellipse tempE = new Ellipse();
            tempE.Height = 10;
            tempE.Width = 10;
            // Create a blue and a black Brush    
            System.Windows.Media.Brush mapable;
            mapable = brush;
            SolidColorBrush blackBrush = new SolidColorBrush();
            blackBrush.Color = Colors.Black;
            // Set Ellipse's width and color    
            tempE.StrokeThickness = 2;
            tempE.Stroke = blackBrush;
            // Fill rectangle with blue color    
            tempE.Fill = mapable;
            return tempE;
        }
        public void end()
        {
            timer.Stop();
            Holder.Children.Clear();
            Draw_PolyLine();
            fill = new SolidColorBrush(Colors.Red);
        }
        /// <summary>
        /// removes the currently editted elispe's from the canvas
        /// </summary>
        private void remove_Elipse()
        {
            for (int itr = 0; itr < Holder.Children.Count; itr++)
            {
                if (Holder.Children[itr] is Ellipse)
                {
                    if (Holder.Children[itr] == Selected_elipse)
                    {
                        Holder.Children.Remove(Selected_elipse);
                        ellipses.Remove(Selected_elipse);
                    }
                }
            }
        }
        private void relocate()
        {
            Point point = Mouse.GetPosition(Holder);

            X = point.X - (ellipse.Width / 2);
            Y = point.Y - (ellipse.Height / 2);
            X = Math.Floor(X);
            Y = Math.Floor(Y);
            X = boardControl(X, Math.Round(Holder.ActualWidth) - 10);
            Y = boardControl(Y, Math.Round(Holder.ActualHeight) - 10);
            Canvas.SetLeft(ellipse, X);
            Canvas.SetTop(ellipse, Y);
        }
        private double boardControl(double input, double topLimit)
        {
            if (input < 0)
            {
                return 0;
            }
            else if (input > topLimit)
            {
                return topLimit;
            }
            return input;
        }
        public void Draw_PolyLine(/*add in int to say which place to edit*/)
        {
            if ((X > 0) && (Y > 0))
            {
                    Polylines.Remove(_Line);
                    _Line = new Polyline();
                    _Line.Stroke = System.Windows.Media.Brushes.AntiqueWhite;
                    List<Point> Lpoints = new List<Point>();
                    Lpoints.Add(new Point(10, 50));
                    Lpoints.Add(new Point(X, Y));
                    Lpoints.Add(new Point(150, 150));
                    PointCollection collection = new PointCollection();
                    foreach (Point i in Lpoints)
                    {
                        collection.Add(i);
                    }
                    _Line.Points = collection;
                    _Line.StrokeThickness = Line_Size;
                     Polylines.Add(_Line);
                    render_lines();
                
            }
        }

        public void selectNode(int index)
        {
            Selected_elipse = ellipses[index];
        }
        public void Set_Line(Polyline Current_line, List<Polyline> Line_List)
        {
            _Line = Current_line;
            Polylines = Line_List;
        }

        private void render_lines()
        {
            foreach (Polyline i in Polylines)
            {
                Holder.Children.Add(i);
            }
        }
    }
}

