﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using Xceed.Wpf.AvalonDock.Layout;


namespace corner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Vars
   

        RealTime_Render realTime_;
        List<Polyline> polylines;
        Polyline Selected_Line;
      
        #endregion
        #region constructor
        public MainWindow()
        {
            polylines = new List<Polyline>();
            InitializeComponent();
            realTime_ = new RealTime_Render(ref testBed);
            DataContext = realTime_;
        }
        #endregion
        private void click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (!realTime_.stopper)
                {
                    realTime_.stopper = true;
                    realTime_.start_timer(sender);
                }
                else
                {
                    realTime_.stopper = false;

                }
            } catch (Exception ex)
            {

            }
        }
        private void Thickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            realTime_.Draw_PolyLine();
        }

        private void Elipse_list_DropDownOpened(object sender, EventArgs e)
        {
            elipse_list.Items.Clear();
            for (int itr = 0; itr < polylines.Count; itr++)
            {
                elipse_list.Items.Add(itr);
            }
        }

        private void Add_Line_Click(object sender, RoutedEventArgs e)
        {
            polylines.Add(new Polyline());
        }

        private void Set_point_1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Set_point_2_Click(object sender, RoutedEventArgs e)
        {
            realTime_.Set_Line(polylines[elipse_list.SelectedIndex], polylines);
        }

        private void Set_point_3_Click(object sender, RoutedEventArgs e)
        {

        }
        private void update_Sel()
        {
            Selected_Line = polylines[(int)elipse_list.SelectedItem];
        }

    }
}   