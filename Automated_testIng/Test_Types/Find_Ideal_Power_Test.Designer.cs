﻿namespace Automated_testIng
{
    partial class Find_Ideal_Power_Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Find_Ideal_Power_Test));
            this.Button_createList = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Single_Ghz_Input = new System.Windows.Forms.NumericUpDown();
            this.End_Freq = new System.Windows.Forms.NumericUpDown();
            this.Step_input = new System.Windows.Forms.NumericUpDown();
            this.Power_input = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.Tolerance_input = new System.Windows.Forms.NumericUpDown();
            this.Aim_input = new System.Windows.Forms.Label();
            this.Target_input = new System.Windows.Forms.NumericUpDown();
            this.inputs_panel = new System.Windows.Forms.Panel();
            this.Number_Of_Loops_Input = new System.Windows.Forms.NumericUpDown();
            this.MiddleFind_Panel = new System.Windows.Forms.Panel();
            this.Start_Ghz_Input = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.max_power_set = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Start_panel = new System.Windows.Forms.Panel();
            this.Loop_On_Off = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wipeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.powerMeterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newConfigToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.currentConfigToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Save_State = new System.Windows.Forms.SaveFileDialog();
            this.Save_Dialog_2 = new System.Windows.Forms.SaveFileDialog();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Single_Ghz_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.End_Freq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Step_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Power_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tolerance_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Target_input)).BeginInit();
            this.inputs_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Number_Of_Loops_Input)).BeginInit();
            this.MiddleFind_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Start_Ghz_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_power_set)).BeginInit();
            this.panel2.SuspendLayout();
            this.Start_panel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Button_createList
            // 
            this.Button_createList.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_createList.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Button_createList.Location = new System.Drawing.Point(117, 0);
            this.Button_createList.Name = "Button_createList";
            this.Button_createList.Size = new System.Drawing.Size(109, 25);
            this.Button_createList.TabIndex = 0;
            this.Button_createList.Text = "Create List";
            this.Button_createList.UseVisualStyleBackColor = true;
            this.Button_createList.Click += new System.EventHandler(this.ListCreateClick);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Location = new System.Drawing.Point(19, 11);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(284, 33);
            this.Title.TabIndex = 1;
            this.Title.Text = "Find Idea Power Test";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Fequency Ghz";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(106, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "End Fequency Ghz";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(11, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Jump Ghz";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(8, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 21);
            this.label4.TabIndex = 2;
            this.label4.Text = "Input Power dBm";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Single_Ghz_Input
            // 
            this.Single_Ghz_Input.DecimalPlaces = 3;
            this.Single_Ghz_Input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Single_Ghz_Input.Location = new System.Drawing.Point(4, 177);
            this.Single_Ghz_Input.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.Single_Ghz_Input.Name = "Single_Ghz_Input";
            this.Single_Ghz_Input.Size = new System.Drawing.Size(76, 21);
            this.Single_Ghz_Input.TabIndex = 3;
            // 
            // End_Freq
            // 
            this.End_Freq.DecimalPlaces = 3;
            this.End_Freq.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.End_Freq.Location = new System.Drawing.Point(111, 42);
            this.End_Freq.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.End_Freq.Name = "End_Freq";
            this.End_Freq.Size = new System.Drawing.Size(76, 21);
            this.End_Freq.TabIndex = 3;
            this.End_Freq.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // Step_input
            // 
            this.Step_input.DecimalPlaces = 3;
            this.Step_input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Step_input.Location = new System.Drawing.Point(15, 42);
            this.Step_input.Name = "Step_input";
            this.Step_input.Size = new System.Drawing.Size(76, 21);
            this.Step_input.TabIndex = 3;
            this.Step_input.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // Power_input
            // 
            this.Power_input.DecimalPlaces = 3;
            this.Power_input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Power_input.Location = new System.Drawing.Point(4, 42);
            this.Power_input.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.Power_input.Name = "Power_input";
            this.Power_input.Size = new System.Drawing.Size(76, 21);
            this.Power_input.TabIndex = 3;
            this.Power_input.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(5, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 21);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tolerance dBm";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Tolerance_input
            // 
            this.Tolerance_input.DecimalPlaces = 3;
            this.Tolerance_input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Tolerance_input.Location = new System.Drawing.Point(4, 85);
            this.Tolerance_input.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.Tolerance_input.Name = "Tolerance_input";
            this.Tolerance_input.Size = new System.Drawing.Size(76, 21);
            this.Tolerance_input.TabIndex = 3;
            this.Tolerance_input.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Aim_input
            // 
            this.Aim_input.AutoSize = true;
            this.Aim_input.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.Aim_input.ForeColor = System.Drawing.Color.White;
            this.Aim_input.Location = new System.Drawing.Point(5, 108);
            this.Aim_input.Name = "Aim_input";
            this.Aim_input.Size = new System.Drawing.Size(164, 21);
            this.Aim_input.TabIndex = 2;
            this.Aim_input.Text = "Target output  dBm";
            this.Aim_input.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Target_input
            // 
            this.Target_input.DecimalPlaces = 3;
            this.Target_input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Target_input.Location = new System.Drawing.Point(4, 132);
            this.Target_input.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.Target_input.Name = "Target_input";
            this.Target_input.Size = new System.Drawing.Size(76, 21);
            this.Target_input.TabIndex = 3;
            this.Target_input.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // inputs_panel
            // 
            this.inputs_panel.Controls.Add(this.Number_Of_Loops_Input);
            this.inputs_panel.Controls.Add(this.Single_Ghz_Input);
            this.inputs_panel.Controls.Add(this.Aim_input);
            this.inputs_panel.Controls.Add(this.label1);
            this.inputs_panel.Controls.Add(this.Target_input);
            this.inputs_panel.Controls.Add(this.label5);
            this.inputs_panel.Controls.Add(this.Tolerance_input);
            this.inputs_panel.Controls.Add(this.MiddleFind_Panel);
            this.inputs_panel.Controls.Add(this.max_power_set);
            this.inputs_panel.Controls.Add(this.Power_input);
            this.inputs_panel.Controls.Add(this.label8);
            this.inputs_panel.Controls.Add(this.label7);
            this.inputs_panel.Controls.Add(this.label4);
            this.inputs_panel.Location = new System.Drawing.Point(0, 166);
            this.inputs_panel.Name = "inputs_panel";
            this.inputs_panel.Size = new System.Drawing.Size(489, 200);
            this.inputs_panel.TabIndex = 4;
            // 
            // Number_Of_Loops_Input
            // 
            this.Number_Of_Loops_Input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Number_Of_Loops_Input.Location = new System.Drawing.Point(159, 42);
            this.Number_Of_Loops_Input.Name = "Number_Of_Loops_Input";
            this.Number_Of_Loops_Input.Size = new System.Drawing.Size(80, 21);
            this.Number_Of_Loops_Input.TabIndex = 8;
            // 
            // MiddleFind_Panel
            // 
            this.MiddleFind_Panel.Controls.Add(this.Start_Ghz_Input);
            this.MiddleFind_Panel.Controls.Add(this.label2);
            this.MiddleFind_Panel.Controls.Add(this.label6);
            this.MiddleFind_Panel.Controls.Add(this.End_Freq);
            this.MiddleFind_Panel.Controls.Add(this.label3);
            this.MiddleFind_Panel.Controls.Add(this.Step_input);
            this.MiddleFind_Panel.Location = new System.Drawing.Point(229, 84);
            this.MiddleFind_Panel.Name = "MiddleFind_Panel";
            this.MiddleFind_Panel.Size = new System.Drawing.Size(260, 111);
            this.MiddleFind_Panel.TabIndex = 7;
            // 
            // Start_Ghz_Input
            // 
            this.Start_Ghz_Input.DecimalPlaces = 3;
            this.Start_Ghz_Input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Start_Ghz_Input.Location = new System.Drawing.Point(15, 87);
            this.Start_Ghz_Input.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.Start_Ghz_Input.Name = "Start_Ghz_Input";
            this.Start_Ghz_Input.Size = new System.Drawing.Size(76, 21);
            this.Start_Ghz_Input.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(11, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 21);
            this.label6.TabIndex = 9;
            this.label6.Text = "Start Fequency Ghz";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // max_power_set
            // 
            this.max_power_set.DecimalPlaces = 3;
            this.max_power_set.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.max_power_set.Location = new System.Drawing.Point(389, 42);
            this.max_power_set.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.max_power_set.Name = "max_power_set";
            this.max_power_set.Size = new System.Drawing.Size(76, 21);
            this.max_power_set.TabIndex = 3;
            this.max_power_set.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(155, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(212, 21);
            this.label7.TabIndex = 2;
            this.label7.Text = "Number Of Loops Allowed";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Title);
            this.panel2.Location = new System.Drawing.Point(12, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(356, 53);
            this.panel2.TabIndex = 5;
            // 
            // Start_panel
            // 
            this.Start_panel.Controls.Add(this.Loop_On_Off);
            this.Start_panel.Controls.Add(this.Button_createList);
            this.Start_panel.Location = new System.Drawing.Point(491, 336);
            this.Start_panel.Name = "Start_panel";
            this.Start_panel.Size = new System.Drawing.Size(227, 30);
            this.Start_panel.TabIndex = 6;
            // 
            // Loop_On_Off
            // 
            this.Loop_On_Off.AutoSize = true;
            this.Loop_On_Off.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loop_On_Off.Location = new System.Drawing.Point(4, 3);
            this.Loop_On_Off.Name = "Loop_On_Off";
            this.Loop_On_Off.Size = new System.Drawing.Size(114, 20);
            this.Loop_On_Off.TabIndex = 8;
            this.Loop_On_Off.Text = "Frequency Loop";
            this.Loop_On_Off.UseVisualStyleBackColor = true;
            this.Loop_On_Off.CheckedChanged += new System.EventHandler(this.Loop_On_Off_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.configureToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(715, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.wipeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.saveToolStripMenuItem.Text = "save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // wipeToolStripMenuItem
            // 
            this.wipeToolStripMenuItem.Name = "wipeToolStripMenuItem";
            this.wipeToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.wipeToolStripMenuItem.Text = "New";
            this.wipeToolStripMenuItem.Click += new System.EventHandler(this.WipeToolStripMenuItem_Click);
            // 
            // configureToolStripMenuItem
            // 
            this.configureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lOToolStripMenuItem,
            this.powerMeterToolStripMenuItem});
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            this.configureToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.configureToolStripMenuItem.Text = "Configure";
            // 
            // lOToolStripMenuItem
            // 
            this.lOToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newConfigToolStripMenuItem,
            this.currentConfigToolStripMenuItem});
            this.lOToolStripMenuItem.Name = "lOToolStripMenuItem";
            this.lOToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.lOToolStripMenuItem.Text = "Signal Generator";
            // 
            // newConfigToolStripMenuItem
            // 
            this.newConfigToolStripMenuItem.Name = "newConfigToolStripMenuItem";
            this.newConfigToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.newConfigToolStripMenuItem.Text = "New Config";
            this.newConfigToolStripMenuItem.Click += new System.EventHandler(this.newSGConfigToolStripMenuItem_Click);
            // 
            // currentConfigToolStripMenuItem
            // 
            this.currentConfigToolStripMenuItem.Name = "currentConfigToolStripMenuItem";
            this.currentConfigToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.currentConfigToolStripMenuItem.Text = "Current Config";
            this.currentConfigToolStripMenuItem.Click += new System.EventHandler(this.SGConfigVeiw_Click);
            // 
            // powerMeterToolStripMenuItem
            // 
            this.powerMeterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newConfigToolStripMenuItem1,
            this.currentConfigToolStripMenuItem1});
            this.powerMeterToolStripMenuItem.Name = "powerMeterToolStripMenuItem";
            this.powerMeterToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.powerMeterToolStripMenuItem.Text = "Power Meter";
            // 
            // newConfigToolStripMenuItem1
            // 
            this.newConfigToolStripMenuItem1.Name = "newConfigToolStripMenuItem1";
            this.newConfigToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.newConfigToolStripMenuItem1.Text = "New Config";
            this.newConfigToolStripMenuItem1.Click += new System.EventHandler(this.newConfigToolStripMenuItem1_Click);
            // 
            // currentConfigToolStripMenuItem1
            // 
            this.currentConfigToolStripMenuItem1.Name = "currentConfigToolStripMenuItem1";
            this.currentConfigToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.currentConfigToolStripMenuItem1.Text = "Current Config";
            this.currentConfigToolStripMenuItem1.Click += new System.EventHandler(this.currentConfigToolStripMenuItem1_Click);
            // 
            // Save_State
            // 
            this.Save_State.CheckPathExists = false;
            this.Save_State.CreatePrompt = true;
            this.Save_State.DefaultExt = "txt";
            this.Save_State.FileName = "setup";
            // 
            // Save_Dialog_2
            // 
            this.Save_Dialog_2.CreatePrompt = true;
            this.Save_Dialog_2.FileName = "output";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Underline);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(373, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 21);
            this.label8.TabIndex = 2;
            this.label8.Text = "Max Power";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Find_Ideal_Power_Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(715, 362);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.Start_panel);
            this.Controls.Add(this.inputs_panel);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Find_Ideal_Power_Test";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mesuro Automated TestBed";
            ((System.ComponentModel.ISupportInitialize)(this.Single_Ghz_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.End_Freq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Step_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Power_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tolerance_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Target_input)).EndInit();
            this.inputs_panel.ResumeLayout(false);
            this.inputs_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Number_Of_Loops_Input)).EndInit();
            this.MiddleFind_Panel.ResumeLayout(false);
            this.MiddleFind_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Start_Ghz_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_power_set)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Start_panel.ResumeLayout(false);
            this.Start_panel.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Button_createList;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown Single_Ghz_Input;
        private System.Windows.Forms.NumericUpDown End_Freq;
        private System.Windows.Forms.NumericUpDown Step_input;
        private System.Windows.Forms.NumericUpDown Power_input;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown Tolerance_input;
        private System.Windows.Forms.Label Aim_input;
        private System.Windows.Forms.NumericUpDown Target_input;
        private System.Windows.Forms.Panel inputs_panel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel Start_panel;
        private System.Windows.Forms.Panel MiddleFind_Panel;
        private System.Windows.Forms.CheckBox Loop_On_Off;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog Save_State;
        private System.Windows.Forms.SaveFileDialog Save_Dialog_2;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currentConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powerMeterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newConfigToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem currentConfigToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem wipeToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown Start_Ghz_Input;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown Number_Of_Loops_Input;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown max_power_set;
        private System.Windows.Forms.Label label8;
    }
}

