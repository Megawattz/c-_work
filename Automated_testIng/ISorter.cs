﻿using System;
using System.Collections.Generic;

namespace Automated_testIng
{
    interface ISorter
    {
        List<string> end_values { get; set; }
        List<Tuple<double, double>> Starting_Values { get; set; }

        /// <summary>
        /// where main list functions should be called to process infomation in the list
        /// </summary>
        void ProcessList();

        List<string> returnList();
    }
}