﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Automated_testIng;
using RapidUtilities;
using Rapid.Utilities;
using RLPCalibrationApplication.Classes.Instruments;
using PowerMeter;
using SignalGenerator;

namespace Automated_testIng
{
    public partial class starting_form : Form
    {
        //double Start_GHz;//for start of loop    
        //double End_GHz;//for end of loop
        //double Step_GHz;// for how much to increment loop by
        //double Power_dBm;// passing power though 
        //double Tolerance;//sets the tolerance of the output that it is acceptable 
        //double Aim;//the target power output we would like 
        //InstrumentInfo SigGenInfo;
        //InstrumentInfo Pow_Met_Info;
        MyTestSetup test_1;
        Exception ErrorMessage;


        //Action Worker_1;
        List<string> Readin;
        Controller _controller;


        //constuctor
        public starting_form()
        {
            InitializeComponent();
            startRead();
            Readin = new List<string>();
            test_1 = new MyTestSetup();
            //    InstrumentInfo Info = new InstrumentInfo() { ConnectionString = "x", DriverName = powermeterDrivers[0] };
            //  PowerMeterDriver P =  InstrumentHelper.ConnectPowerMeter(Info);
        }
        void startRead()
        {
            using (wait_dialogcs waitbox = new wait_dialogcs(readRegistry, false))
            {
                waitbox.ShowDialog(this);
            }
        }
        //reads the windows registry and returns all useable hardware in catergorised Lists
        void readRegistry()
        {
            FMWcom.GetInstrumentCatagories();
        }
        //creates and uses a instance of controller class and uses it to run calcualtions plus passes though drivers to the controller to use
        private void ListCreateClick(object sender, EventArgs e)
        {
            //make sure all values are correct at run time
            test_1.Start_GHz = (double)Start_Freq.Value;
            test_1.End_GHz = (double)End_Freq.Value;
            test_1.Power_dBm = (double)Power_input.Value;
            test_1.Step_GHz = (double)Step_input.Value;
            test_1.Tolerance = (double)Tolerance_input.Value;
            test_1.Aim = (double)Target_input.Value;
            test_1.loopNumber = 20;

            using (wait_dialogcs waitbox = new wait_dialogcs(testLoop, true))
            {
                waitbox.ShowDialog(this);
            }

            if (ErrorMessage == null)
            {
                saveList("csv", _controller.output);
                saveList("csv", _controller.ideal_Power_Output);
                
            }
            else
            {
                MessageBox.Show(ErrorMessage.Message + Environment.NewLine + "No Results made");
            }
        }
        void testLoop()
        {
            ErrorMessage = null;
            try
            {
                //creates the controller class and feeds it the stats currently set in the numberboxes
                _controller = new Controller(test_1);

                for (int i = 0; i <= _controller.loopSize; i++)
                {
                   
                    double currentGHz = test_1.Start_GHz + (i * test_1.Step_GHz);
                    if (i == _controller.loopSize)
                    {
                        currentGHz = test_1.End_GHz;
                    }
                    _controller.currentGHz = currentGHz;
                    //makes us read full power output for all the frequencies range the user has inputed
                    _controller.CalculatePowerOut();
                    _controller.runTest();
                }


                if (Target_On_Off.Checked)
                {
                    //makes the list in cvs file format
                    _controller.CreatResultsList();
                }
                else
                {
                    _controller.CreateBasicResults();
                }

                MessageBox.Show("testing at " + test_1.Power_dBm + "dBm has completed");

            }
            catch (Exception ex)
            {

                ErrorMessage = ex;
            }
        }

        //tempory to set values to save time debugging
        private void starting_form_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// removes target feild from results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void remove_target(object sender, EventArgs e)
        {
            if (Target_On_Off.Checked)
            {
                MiddleFind_Panel.Visible = true;
            }
            else
            {
                MiddleFind_Panel.Visible = false;
            }
        }
        /// <summary>
        /// if to get config form to show when the user has clicked on the config tab at the top of the form
        /// </summary>
        /// <param name="sender">object that sent the request</param>
        /// <param name="e">event handler</param>
        private void newSGConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Config_SG SGInfo = new Config_SG();
            if (SGInfo.ShowDialog(this) == DialogResult.Yes)
            {
                test_1.SigGenInfo = SGInfo.SigGenInfo;
            }
            else
            {
                MessageBox.Show("the form has been cancelled");
            }
        }
        /// <summary>
        /// shows current configeration of Signal Generator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="x"></param>
        private void SGConfigVeiw_Click(object sender, EventArgs x)
        {

            try
            {

                MessageBox.Show("current Hardware: " + test_1.SigGenInfo.DriverName.ToString() + Environment.NewLine + "Current Connection String: " + test_1.SigGenInfo.ConnectionString.ToString());
            }
            catch (System.NullReferenceException)
            {
                MessageBox.Show("no configuration pressent!" + Environment.NewLine + "Please load on or configure drivers");
            }
        }


        /// <summary>
        /// runs config power form and if returns correctly then fills in info on powerMeter info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newConfigToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //create instance of config form
            PowerMeter_Configcs PMconfig = new PowerMeter_Configcs();

            //check what it returns and do the code in the if, only if the dialogResult comes back as what you want in this case ok
            if (PMconfig.ShowDialog(this) == DialogResult.Yes)
            {
                test_1.Pow_Met_Info = PMconfig.Power_Meter_Info;
            }
            else
                MessageBox.Show("process was cancelled");
        }

        /// <summary>
        /// shows current config of PowerMeter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void currentConfigToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("PowerMeter Driver: " + test_1.Pow_Met_Info.DriverName.ToString() + Environment.NewLine + "Connection string: " + test_1.Pow_Met_Info.ConnectionString.ToString());
            }
            catch (System.NullReferenceException)
            {
                MessageBox.Show("no configuration pressent!" + Environment.NewLine + "Please load on or configure drivers");
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save_State.Title = "save Current settings";
            Save_State.InitialDirectory = "C:\\Users\\jonny\\Documents";
            Save_State.DefaultExt = "XML";

            //make sure file has a name before saving it
            if (Save_State.ShowDialog() == DialogResult.OK)
            {
                string filePath = Save_State.FileName;
                string FileData = XMLIO.SerializeToString<MyTestSetup>(test_1);

                System.IO.File.WriteAllText(filePath, FileData);
            }
        }
        //load in setting saved before
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog Load_in = new OpenFileDialog();
            if (Load_in.ShowDialog() == DialogResult.OK)
            {
                string Intake = System.IO.File.ReadAllText(Load_in.FileName);
                test_1 = XMLIO.DeSerializeFromString<MyTestSetup>(Intake);
            }
        }

        //used to save outputs at the end of task
        private void saveList(string fileExtention, List<string> ToBeSaved)
        {
            Save_Dialog_2.Title = "Save List in document";
            Save_Dialog_2.DefaultExt = fileExtention;

            if (Save_Dialog_2.ShowDialog() == DialogResult.OK)
            {
                string filepath = Save_Dialog_2.FileName;
                System.IO.File.WriteAllLines(filepath, ToBeSaved);
            }
        }
    }
}


