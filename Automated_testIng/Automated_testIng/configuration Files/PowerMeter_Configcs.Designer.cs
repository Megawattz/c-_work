﻿namespace Automated_testIng
{
    partial class PowerMeter_Configcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PowerMeter_Configcs));
            this.input_list = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Done_Button = new System.Windows.Forms.Button();
            this.Cancel_Button = new System.Windows.Forms.Button();
            this.Main_inputs_pannel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.buttons_pannel = new System.Windows.Forms.Panel();
            this.logos_visual_pannel = new System.Windows.Forms.Panel();
            this.Main_inputs_pannel.SuspendLayout();
            this.buttons_pannel.SuspendLayout();
            this.SuspendLayout();
            // 
            // input_list
            // 
            this.input_list.BackColor = System.Drawing.Color.SlateGray;
            this.input_list.CausesValidation = false;
            this.input_list.FormattingEnabled = true;
            this.input_list.Location = new System.Drawing.Point(3, 22);
            this.input_list.Name = "input_list";
            this.input_list.Size = new System.Drawing.Size(277, 25);
            this.input_list.TabIndex = 0;
            this.input_list.SelectedValueChanged += new System.EventHandler(this.input_list_SelectedValueChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.SlateGray;
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(300, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(339, 24);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(309, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Connection String Used";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Signal Generator Used";
            // 
            // Done_Button
            // 
            this.Done_Button.BackColor = System.Drawing.Color.SlateGray;
            this.Done_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Done_Button.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Done_Button.ForeColor = System.Drawing.Color.White;
            this.Done_Button.Location = new System.Drawing.Point(142, 7);
            this.Done_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Done_Button.Name = "Done_Button";
            this.Done_Button.Size = new System.Drawing.Size(136, 42);
            this.Done_Button.TabIndex = 0;
            this.Done_Button.Text = "Save";
            this.Done_Button.UseVisualStyleBackColor = false;
            this.Done_Button.Click += new System.EventHandler(this.Finishe_button_clicked);
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.BackColor = System.Drawing.Color.SlateGray;
            this.Cancel_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancel_Button.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Cancel_Button.ForeColor = System.Drawing.Color.White;
            this.Cancel_Button.Location = new System.Drawing.Point(7, 7);
            this.Cancel_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(136, 42);
            this.Cancel_Button.TabIndex = 0;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = false;
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // Main_inputs_pannel
            // 
            this.Main_inputs_pannel.Controls.Add(this.input_list);
            this.Main_inputs_pannel.Controls.Add(this.textBox1);
            this.Main_inputs_pannel.Controls.Add(this.label2);
            this.Main_inputs_pannel.Controls.Add(this.label3);
            this.Main_inputs_pannel.Location = new System.Drawing.Point(2, 54);
            this.Main_inputs_pannel.Name = "Main_inputs_pannel";
            this.Main_inputs_pannel.Size = new System.Drawing.Size(651, 54);
            this.Main_inputs_pannel.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.label1.Location = new System.Drawing.Point(-1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(362, 33);
            this.label1.TabIndex = 3;
            this.label1.Text = "PowerMeter  Comfig Form";
            // 
            // buttons_pannel
            // 
            this.buttons_pannel.Controls.Add(this.Cancel_Button);
            this.buttons_pannel.Controls.Add(this.Done_Button);
            this.buttons_pannel.Location = new System.Drawing.Point(366, 313);
            this.buttons_pannel.Name = "buttons_pannel";
            this.buttons_pannel.Size = new System.Drawing.Size(286, 58);
            this.buttons_pannel.TabIndex = 4;
            // 
            // logos_visual_pannel
            // 
            this.logos_visual_pannel.Location = new System.Drawing.Point(2, 313);
            this.logos_visual_pannel.Name = "logos_visual_pannel";
            this.logos_visual_pannel.Size = new System.Drawing.Size(364, 57);
            this.logos_visual_pannel.TabIndex = 5;
            // 
            // PowerMeter_Configcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(654, 371);
            this.Controls.Add(this.logos_visual_pannel);
            this.Controls.Add(this.buttons_pannel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Main_inputs_pannel);
            this.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PowerMeter_Configcs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Power Meter Config";
            this.Main_inputs_pannel.ResumeLayout(false);
            this.Main_inputs_pannel.PerformLayout();
            this.buttons_pannel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox input_list;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Done_Button;
        private System.Windows.Forms.Button Cancel_Button;
        private System.Windows.Forms.Panel Main_inputs_pannel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel buttons_pannel;
        private System.Windows.Forms.Panel logos_visual_pannel;
    }
}