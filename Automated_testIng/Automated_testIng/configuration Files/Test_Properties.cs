﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidUtilities;
using Rapid.Utilities;
using RLPCalibrationApplication.Classes.Instruments;

namespace Automated_testIng
{
    public class Test_Properties
    {
       public double Start_GHz;//for start of loop    
       public double End_GHz;//for end of loop
       public double Step_GHz;// for how much to increment loop by
       public double Power_dBm;// passing power though 
       public double Tolerance;//sets the tolerance of the output that it is acceptable 
       public  double Aim;//the target power output we would like 
       public double loopNumber;//the number of the loops we do in the Ideal Frequency test
       public InstrumentInfo SigGenInfo;
       public InstrumentInfo Pow_Met_Info;
        public bool Is_Loop_True;
    }
}
