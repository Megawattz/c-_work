﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rapid.Instruments.SignalGenerators;
using System.Drawing.Text;


namespace Automated_testIng
{
    class Dummy_LO_Source : ILocalOscillatorSource

    {
        double GHz;
        double dBm;
        bool RF_status;

        public void closeConnection()
        {
            
        }

        public double getFrequencyGHz()
        {
            return GHz;
        }

        public double getPowerLevel()
        {
            return dBm;
        }

        public bool getRFStatus()
        {
            return RF_status;
        }

        public bool isConnectionValid()
        {
            return true;
        }

        public void set10MHzReference(bool isInternal = true)
        {
            
        }

        public void setConnectionString(string connectionString = null, int chNum = 0)
        {
            
        }

        //reads in GHz
        public void setFrequencyGHz(double frequency)
        {
            GHz = frequency;
            FakeReading();
        }

        public void setLOSource(double loFrequencyGHz, double powerLevel = 10, bool rfStatus = true, bool isInternalRef = false)
        {
            
        }

        //reads in dBm
        public void setPowerLevel(double powerLevel)
        {
            dBm = powerLevel;
            FakeReading();
        }

        //reads in Rf status
        public void setRFStatus(bool rfStatus)
        {
            RF_status = rfStatus;
            FakeReading();
        }

        //create a function to dump out infomation to a txt document
        void FakeReading()
        {
            System.IO.File.WriteAllLines("../../FakeReadings.txt", new List<string> { GHz.ToString() , dBm.ToString(), RF_status.ToString(), "Alive" });
            //System.Threading.Thread.Sleep(50);
        }
    }


}