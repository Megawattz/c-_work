﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RapidUtilities;
using Rapid.Utilities;
using RLPCalibrationApplication.Classes.Instruments;

namespace Automated_testIng
{
    public partial class PowerMeter_Configcs : Form
    {
        private string currentString;
        private string currentInstrument;
        List<string> AvaibleDrivers;

        public InstrumentInfo Power_Meter_Info
        {
            get
            {
                return new InstrumentInfo() { ConnectionString = this.currentString, DriverName = currentInstrument,DriverType = DriverTypes.FOCUSCOM.ToString() };
            }
        }
        public PowerMeter_Configcs()
        {
            InitializeComponent();
            currentString = "";
            currentInstrument = "";
            AvaibleDrivers = new List<string>();
            AvaibleDrivers = FMWcom.GetDriverStrings(InstrumentTypes.PowerMeter).ToList<string>();
            input_list.DataSource = AvaibleDrivers;
        }

        private void input_list_SelectedValueChanged(object sender, EventArgs e)
        {
            currentInstrument = input_list.SelectedItem.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            currentString = textBox1.Text;
        }

        private void Finishe_button_clicked(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
