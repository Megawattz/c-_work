﻿using PowerMeter;
using SignalGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automated_testIng
{
    public class Find_Input_Power
    {

        public static double FindPowerLoop(MyTestSetup setup, IPowerMeterDriver powerMeterDriver, SignalGeneratorDriver signalGeneratorDriver, double Ghz)
        {
            double output = 0;
            double input = setup.Power_dBm;
            bool takeaway = false;
            for (int i = 0; i <= setup.loopNumber; i++)
            {
                takeaway = false;
                //read with with current settings
                //setup the SG for the current power we have
                if (Ghz == 0)
                    signalGeneratorDriver.setFreq(0.1);
                else
                {
                    signalGeneratorDriver.setFreq(Ghz);
                }
                signalGeneratorDriver.setPower(input);
                signalGeneratorDriver.onOff(1);

                //get the power reading from the powermeter
                powerMeterDriver.configure(1, Ghz);
                output = powerMeterDriver.readPower(1);

                //caluacte the differance between the target and actual output
                double differance = setup.Aim - output;
                if (differance < 0)
                {
                    differance = output - setup.Aim;
                    takeaway = true;
                }

                //if the differance is more than half the tolerance  then we are within tolerance therefore the input is good
                if (setup.Tolerance / 2 > differance)
                {
                    break;
                }
                if (takeaway)
                {
                    input = input - differance;
                }
                else
                {
                    input = input + differance;
                }
            }
            return input;
        }
    }
}
