﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Automated_testIng;
using RapidUtilities;
using Rapid.Utilities;
using RLPCalibrationApplication.Classes.Instruments;
using PowerMeter;
using SignalGenerator;

namespace Automated_testIng
{
    public partial class Find_Ideal_Power_Test : Form
    {
        Test_Properties Current_Infomation;
        string Input_Properties;
        Test_interpreter _Interpreter;
        Exception ErrorMessage;
        //Action Worker_1;
        List<string> Readin;
        Controller _controller;


        //constuctor
        public Find_Ideal_Power_Test(string test)
        {
            InitializeComponent();
            StartRead();
            _Interpreter = new Test_interpreter();
            Readin = new List<string>();
            Current_Infomation = new Test_Properties();
            Input_Properties = test;
            if (test != " ")
                Loadin();
            Remove_Loop_Options();
            //    InstrumentInfo Info = new InstrumentInfo() { ConnectionString = "x", DriverName = powermeterDrivers[0] };
            //  PowerMeterDriver P =  InstrumentHelper.ConnectPowerMeter(Info);
        }
        void StartRead()
        {
            using (wait_dialogcs waitbox = new wait_dialogcs(ReadRegistry, false))
            {
                waitbox.ShowDialog(this);
            }
        }
        //reads the windows registry and returns all useable hardware in catergorised Lists
        void ReadRegistry()
        {
            FMWcom.GetInstrumentCatagories();
        }
        //creates and uses a instance of controller class and uses it to run calcualtions plus passes though drivers to the controller to use
        private void ListCreateClick(object sender, EventArgs e)
        {
            //make sure all values are correct
            Valid_Values();
            //create waitbox and make it run test within custom thread
            using (wait_dialogcs waitbox = new wait_dialogcs(TestLoop, true))
            {
                waitbox.ShowDialog(this);
            }
            //if theres any error catch it 
            if (ErrorMessage == null)
            {
                saveList("csv", _controller.output);
            }
            else
            {
                MessageBox.Show(ErrorMessage.Message + Environment.NewLine + "No Results made");
            }
        }

        private void TestLoop()
        {
            ErrorMessage = null;
            Valid_Values();
            
            try
            {
                //creates the controller class and feeds it the stats currently set in the numberboxes
                _controller = new Controller(Current_Infomation);
                if (Loop_On_Off.Checked)
                {
                    for (int i = 0; i <= _controller.loopSize; i++)
                    {

                        double currentGHz = Current_Infomation.Start_GHz + (i * Current_Infomation.Step_GHz);
                        if (i == _controller.loopSize)
                        {
                            currentGHz = Current_Infomation.End_GHz;
                        }
                        _controller.currentGHz = currentGHz;
                        //makes us read full power output for all the frequencies range the user has inputed
                        _controller.runTest();
                    }
                }
                else
                {
                    //run single value output
                    _controller.currentGHz = Current_Infomation.Start_GHz;
                    _controller.runTest();
                }
                    //save the output
                    MessageBox.Show("testing at " + Current_Infomation.Power_dBm + "dBm has completed");
            }
            //error catcher 
            catch (Exception ex)
            {//put out the error message if one was caught
                ErrorMessage = ex;
            }
        }
        /// <summary>
        /// removes target feild from results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Remove_Loop_Options()
        {
            if (!Loop_On_Off.Checked)
            {
                MiddleFind_Panel.Visible = false;
                Single_Ghz_Input.Visible = true;
                label1.Visible = true;
                Current_Infomation.Is_Loop_True = false;
            }
            else
            {
                MiddleFind_Panel.Visible = true;
                Single_Ghz_Input.Visible = false;
                label1.Visible = false;
                Current_Infomation.Is_Loop_True = true;
            }
        }
        /// <summary>
        /// if to get config form to show when the user has clicked on the config tab at the top of the form
        /// </summary>
        /// <param name="sender">object that sent the request</param>
        /// <param name="e">event handler</param>
        private void newSGConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Config_SG SGInfo = new Config_SG();
            if (SGInfo.ShowDialog(this) == DialogResult.Yes)
            {
                Current_Infomation.SigGenInfo = SGInfo.SigGenInfo;
            }
            else
            {
                MessageBox.Show("the form has been cancelled");
            }
        }
        /// <summary>
        /// shows current configeration of Signal Generator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="x"></param>
        private void SGConfigVeiw_Click(object sender, EventArgs x)
        {

            try
            {

                MessageBox.Show("current Hardware: " + Current_Infomation.SigGenInfo.DriverName.ToString() + Environment.NewLine + "Current Connection String: " + Current_Infomation.SigGenInfo.ConnectionString.ToString());
            }
            catch (System.NullReferenceException)
            {
                MessageBox.Show("no configuration pressent!" + Environment.NewLine + "Please load on or configure drivers");
            }
        }


        /// <summary>
        /// runs config power form and if returns correctly then fills in info on powerMeter info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newConfigToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //create instance of config form
            PowerMeter_Configcs PMconfig = new PowerMeter_Configcs();

            //check what it returns and do the code in the if, only if the dialogResult comes back as what you want in this case ok
            if (PMconfig.ShowDialog(this) == DialogResult.Yes)
            {
                Current_Infomation.Pow_Met_Info = PMconfig.Power_Meter_Info;
            }
            else
                MessageBox.Show("process was cancelled");
        }

        /// <summary>
        /// shows current config of PowerMeter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void currentConfigToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("PowerMeter Driver: " + Current_Infomation.Pow_Met_Info.DriverName.ToString() + Environment.NewLine + "Connection string: " + Current_Infomation.Pow_Met_Info.ConnectionString.ToString());
            }
            catch (System.NullReferenceException)
            {
                MessageBox.Show("no configuration pressent!" + Environment.NewLine + "Please load on or configure drivers");
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //setup save dialog and filepath, make a default file type
            Save_State.Title = "save Current settings";
            Save_State.InitialDirectory = "C:\\Users\\jonny\\Documents";
            Save_State.DefaultExt = "XML";
            //make sure values are current 
            Valid_Values();
            //make sure file has a name before saving it
            if (Save_State.ShowDialog() == DialogResult.OK)
            {
                //get the filepath to save to
                string filePath = Save_State.FileName;

                _Interpreter.test_ID = "Find_Ideal_Power_Test";
                _Interpreter.test_Properties = XMLIO.SerializeToString<Test_Properties>(Current_Infomation);

                //add string that takes xml and save the test type too 
                string FileData = XMLIO.SerializeToString<Test_interpreter>(_Interpreter);
                //print out the final result
                System.IO.File.WriteAllText(filePath, FileData);
            }
        }
        //load in setting saved before
        private void Loadin()
        {
            Current_Infomation = XMLIO.DeSerializeFromString<Test_Properties>(Input_Properties);
            showValues();
        }

        //used to save outputs at the end of task
        private void saveList(string fileExtention, List<string> ToBeSaved)
        {
               _controller.output.Insert(0, "GHz, ideal power input dBm");
            Save_Dialog_2.Title = "Save Ideal Power Output File";
            Save_Dialog_2.DefaultExt = fileExtention;

            if (Save_Dialog_2.ShowDialog() == DialogResult.OK)
            {
                string filepath = Save_Dialog_2.FileName;
                System.IO.File.WriteAllLines(filepath, ToBeSaved);
            }
        }

        private void WipeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LandingForm landingForm = new LandingForm();
            landingForm.Show();
            this.Hide();
        }

        //updates all values to what is in the display boxes
        private void Valid_Values()
        {
            Current_Infomation.Is_Loop_True = true;
            Current_Infomation.Start_GHz = (double)Start_Ghz_Input.Value;
            Current_Infomation.End_GHz = (double)End_Freq.Value;
            Current_Infomation.Power_dBm = (double)Power_input.Value;
            Current_Infomation.Step_GHz = (double)Step_input.Value;
            Current_Infomation.Tolerance = (double)Tolerance_input.Value;
            Current_Infomation.Aim = (double)Target_input.Value;
            Current_Infomation.loopNumber = (double)Number_Of_Loops_Input.Value;
            if (!Loop_On_Off.Checked)
            {
                Current_Infomation.Start_GHz = (double)Single_Ghz_Input.Value;
                Current_Infomation.Is_Loop_True = false;
            }
        }
        //updates all tools in form when called
        void showValues()
        {
            Start_Ghz_Input.Value =(decimal) Current_Infomation.Start_GHz;
            End_Freq.Value = (decimal)Current_Infomation.End_GHz;
            Power_input.Value = (decimal)Current_Infomation.Power_dBm;
            Step_input.Value = (decimal)Current_Infomation.Step_GHz;
            Tolerance_input.Value = (decimal)Current_Infomation.Tolerance;
            Target_input.Value = (decimal)Current_Infomation.Aim;
            Number_Of_Loops_Input.Value = (decimal)Current_Infomation.loopNumber;
            if (!Current_Infomation.Is_Loop_True)
            {
                Loop_On_Off.Checked = false;
                Single_Ghz_Input.Value = (decimal)Current_Infomation.Start_GHz;
            }

        }

        private void Loop_On_Off_CheckedChanged(object sender, EventArgs e)
        {
            Remove_Loop_Options();
        }
    }
}


