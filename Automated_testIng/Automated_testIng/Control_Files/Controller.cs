﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rapid.Instruments.SignalGenerators;
using PowerMeter;
using SignalGenerator;
using Rapid;
using RapidUtilities;
using RLPCalibrationApplication.Classes.Instruments;
using System.Windows.Forms;

namespace Automated_testIng
{
    class Controller : IController
    {
        //variables
        double TdBm;
        double tolerance;
        double Aim;
        public double currentGHz;
        List<Tuple<double,double>> input_list;
        public List<string> output;
        public int loopSize;
        public double idealPower;
        public string idealPowerString;
        //drivers for hardware being loaded in
        SignalGeneratorDriver D_LO;
        IPowerMeterDriver D_Power;

        //classes for the calcuation of figures
        Extractor Ghz_RangeEx;
        Sorter FreqecencySorter;
        basic_sorter Basic_Sorter;
        Test_Properties Csetup;


        //constructor
        public Controller(Test_Properties setup)
        {
            if(setup.Is_Loop_True)
                loopSize = loopCalculation(setup.Start_GHz,setup.End_GHz, setup.Step_GHz);
            try
            {
                Csetup = setup;
                TdBm = setup.Power_dBm;
                Aim = setup.Aim;
                tolerance = setup.Tolerance;
                currentGHz = 0;
                idealPower = 0;
                input_list = new List<Tuple<double, double>>();
                output = new List<string>();
                //initalise the hardware drives
                D_LO = InstrumentHelper.ConnectSignalGenerator(setup.SigGenInfo);
                D_Power = InstrumentHelper.ConnectPowerMeter(setup.Pow_Met_Info);             
                //catch an error to end this code and give a error message code
            }
            catch (Exception ex)
            {
                throw new Exception("hardware failed to have drivers created please check configuration", ex);
            }
        }

        //sets the hardware for a step in the user loop then records the outcoming results to a list 
        public void CalculatePowerOut()
        {
            //set the SG up with values passed to the controller when created 
            setSG();
            //get a power reading from the powermeter
            double p = powerReaderReading(currentGHz);
            //add that power reading and the frequency we tested at and store them for future processing
            input_list.Add(new Tuple<double, double>(currentGHz, p));
        }
        /// <summary>
        /// goes to powermeter connected and takes the reading from it.
        /// </summary>
        /// <param name="testGHz"></param>
        /// <returns></returns>
        double powerReaderReading(double testGHz)
        {
            //set the frequency for this test
            D_Power.configure(1, testGHz);
            return D_Power.readPower(1);
        }
        /// <summary>
        ///connects to requestesd Signal Generator and set its values.
        /// </summary>
        void setSG()
        {
            D_LO.setPower(TdBm);
            D_LO.setFreq(currentGHz);
            D_LO.onOff(1);
        }
        /// <summary>
        /// finds the closet power to the power output we wanted
        /// </summary>
        public void findTarget()
        {
            Ghz_RangeEx = new Extractor(input_list,( Aim + tolerance), (Aim -tolerance), Aim);
            //output a nice message and the output as a new line so the user can store values;
            System.IO.File.WriteAllText("../../ Aim_Output.txt","ideal freqeuncy to output " + Aim + " dBm " + " when the input is  " + TdBm + " dBm " + " is " + Ghz_RangeEx.findIdeal().ToString() + " GHz" + Environment.NewLine);
        }
        /// <summary>
        /// swaps valves around if the value that is sopoust to be higher is lower than the other value
        /// </summary>
        /// <param name="High">higher value</param>
        /// <param name="Low">lower value</param>
        void LowerSwap(ref double High, ref double Low)
        {
            //make sure human error
            if (High < Low)
            {
                double temp = High;
                High = Low;
                Low = temp;
            }
        }
        /// <summary>
        /// creates a list that returns results and if the output power is within user tolerances
        /// </summary>
       public void CreatResultsList()
        {
            //create a verison of the sorter class
            FreqecencySorter = new Sorter(tolerance, Aim, input_list);                  

            //make the output list filled with results 
            output = FreqecencySorter.returnList();
            //add tags
            output.Insert(0, "GHz ," + " dBm ," + " within tolerance");
        }
        /// <summary>
        /// returns basic output form
        /// </summary>
        public void CreateBasicResults()
        {
            Basic_Sorter = new basic_sorter(input_list);

            output = Basic_Sorter.returnList();
            output.Insert(0, "GHz, " + "dBm ");                     
        }
        /// <summary>
        /// takes inputs and figures out how many times loop will have to be done 
        /// </summary>
        /// <param name="start_input"></param>
        /// <param name="end_input"></param>
        /// <param name="jump_input"></param>
        /// <returns></returns>
        int loopCalculation(double start_input, double end_input, double jump_input)
        {
            double sizeUnrounded = (end_input - start_input) / jump_input;

            int size = (int) Math.Ceiling(sizeUnrounded);

            return size;
        }
        public void runTest()
        {
            //create a instance of the Find_input_Power object and make it start running though the loop and give us a single double value for the ideal input 
            idealPower = Find_Input_Power.FindPowerLoop(Csetup, D_Power, D_LO, currentGHz);
            idealPowerString = currentGHz.ToString()+","+ idealPower.ToString() ;
            output.Add(idealPowerString);
        }         
      }
}
