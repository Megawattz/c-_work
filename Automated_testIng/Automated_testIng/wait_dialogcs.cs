﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Automated_testIng
{
    /// <summary>
    /// creates a wait box for user to show pocessing is being done in the program
    /// </summary>
    public partial class wait_dialogcs : Form
    {
        /// <summary>
        /// creates a thread to do a task on
        /// </summary>
        public Action Worker { get; private set; }
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="worker">fucntion that you wish to run in the form</param>
        public wait_dialogcs(Action worker, bool hidden)
        {
            InitializeComponent();
            Worker = worker;
            if (hidden)
            {
                this.Visible = false;
            }
        }
        /// <summary>
        /// closes the form after the task has been completed 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(Worker).ContinueWith(t => { Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
