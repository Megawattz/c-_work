﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SignalGenerator;
using PowerMeter;
using DCPowerSupply;
using Rapid.Utilities;

namespace RLPCalibrationApplication.Classes.Instruments
{

    static class InstrumentHelper
    {
        public static readonly Dictionary<InstrumentTypes, string> FriendlyNames = new Dictionary<InstrumentTypes, string>
        {
            { InstrumentTypes.PowerMeter, "Power Meter" },
            { InstrumentTypes.SignalGenerator, "Signal Generator" },
            { InstrumentTypes.RLP_Harmonic_Tuner, "RLP Harmonic Tuner" },
            { InstrumentTypes.RLP_Tuner, "RLP Tuner"},
            { InstrumentTypes.VNA, "VNA" },
            { InstrumentTypes.DCSupply, "DC Supply" }
        };

        // [kw-ToDo] Will need to get this value from the config file later - number of harmonics allowed.
        public static int MAX_HARMONICS = 5;
        public static string HARMONIC = "FO";


        //[KW] Constants for the tuner configuration list view
        public const int LV_FPGA = 1;
        public const int LV_UPDOWNMODULE = 2;
        public const int LV_LOSOURCE = 3;


        #region Static Functions

        public static SignalGeneratorDriver ConnectSignalGenerator(string signalGeneratorInstrStr)
        {
            SignalGeneratorDriver signalGenerator = null;
            string[] connectionParams = signalGeneratorInstrStr.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                signalGenerator = (SignalGeneratorDriver)CreateDriverInstance(connectionParams[0]);
                signalGenerator.setVISAConnectionStr(connectionParams[1]);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create an instance of the signal generator due to an error. Check the connection string.", ex);
            }

            return signalGenerator;
        }

        public static PowerMeterDriver ConnectPowerMeter(string powerMeterDriverInstrStr)
        {
            PowerMeterDriver powerMeter = null;
            string[] connectionParams = powerMeterDriverInstrStr.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                powerMeter = (PowerMeterDriver)CreateDriverInstance(connectionParams[0]);
                powerMeter.setVISAConnectionStr(connectionParams[1]);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create an instance of the power meter due to an error. Check the connection string.", ex);
            }

            return powerMeter;
        }
        #endregion


        public  static SignalGeneratorDriver ConnectSignalGenerator(InstrumentInfo Info)
        {
            SignalGeneratorDriver signalGenerator = null;
            try
            {
                if (Info.DriverType == DriverTypes.FOCUSCOM.ToString())
                {
                    signalGenerator = (SignalGeneratorDriver)CreateDriverInstance(Info.DriverName);
                    signalGenerator.setVISAConnectionStr(Info.ConnectionString);
                }
                else { throw new Exception("Incorrect Driver Type"); }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create an instance of the signal generator due to an error. Check the connection string.", ex);
            }

            return signalGenerator;
        }

        public static PowerMeterDriver ConnectPowerMeter(InstrumentInfo Info)
        {
            PowerMeterDriver powerMeter = null;
 
            try
            {
                if (Info.DriverType == DriverTypes.FOCUSCOM.ToString())
                {
                    powerMeter = (PowerMeterDriver)CreateDriverInstance(Info.DriverName);
                    powerMeter.setVISAConnectionStr(Info.ConnectionString);
                }
                else { throw new Exception("Incorrect Driver Type"); }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create an instance of the power meter due to an error. Check the connection string.", ex);
            }

            return powerMeter;
        }

        public static DCPowerSupplyDriver ConnectDcPowerSupply(InstrumentInfo Info) 
        {
            DCPowerSupplyDriver DCpowerSupply = null;

            try
            {
                if (Info.DriverType == DriverTypes.FOCUSCOM.ToString())
                {
                    DCpowerSupply = (DCPowerSupplyDriver)CreateDriverInstance(Info.DriverName);
                    DCpowerSupply.setVISAConnectionStr(Info.ConnectionString);
                }
                else { throw new Exception("Incorrect Driver Type"); }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create an instance of the  DC Power Supply due to an error. Check the connection string.", ex);
            }

            return DCpowerSupply;
        }
        public static object CreateDriverInstance(string str)
        {
            try
            {
                Type x = Type.GetTypeFromProgID(str);
                var myObj = Activator.CreateInstance(x);
                return myObj;
            }
            catch (Exception e)
            {
                throw new ApplicationException("Failed to create driver: " + str, e);
            }
            //return null;
        }
    }
    public class InstrumentInfo
    {
        string _frendlyName;
        string _driverName;
        string _driverType;
        string _connectionString;
        string _instrumentType;

        public InstrumentInfo()
        {
            _frendlyName = "";
            _driverName = "";
            _driverType = "";
            _connectionString = "";
            _instrumentType = "";
        }


        public string FrendlyName { get { return _frendlyName; } set { _frendlyName = value; } }
        public string DriverName { get { return _driverName; } set { _driverName = value; } }
        public string DriverType { get { return _driverType; } set { _driverType = value; } }
        public string ConnectionString { get { return _connectionString; } set { _connectionString = value; } }
        public string InstrumentType { get { return _instrumentType; } set { _instrumentType = value; } }

        internal string GetSigGenString()
        {
            return DriverName + "|" + ConnectionString;
        }
    }

}


//    public enum InstrumentTypes
//    {
//        PowerMeter = 0,
//        SignalGenerator = 1,
//        RLP_Tuner = 2,
//        VNA = 3,
//        DCSupply = 4,
//        RLP_Harmonic_Tuner = 5,
//    }

//    public enum DriverTypes 
//    {
//        FOCUSCOM =0,
//        INTERNALnet =1,
//        EXTERNALnet =2,
//    }

//    public enum VNAPortNumbers
//    {
//        PORT1 = 1,
//        PORT2 = 2,
//        PORT3 = 3,
//        PORT4 = 4
//    }

//    static class InstrumentHelper
//    {
//        public static readonly Dictionary<InstrumentTypes, string> FriendlyNames = new Dictionary<InstrumentTypes, string>
//        {
//            { InstrumentTypes.PowerMeter, "Power Meter" },
//            { InstrumentTypes.SignalGenerator, "Signal Generator" },
//            { InstrumentTypes.RLP_Harmonic_Tuner, "RLP Harmonic Tuner" },
//            { InstrumentTypes.RLP_Tuner, "RLP Tuner"},
//            { InstrumentTypes.VNA, "VNA" },
//            { InstrumentTypes.DCSupply, "DC Supply" }
//        };

//        // [kw-ToDo] Will need to get this value from the config file later - number of harmonics allowed.
//        public static int MAX_HARMONICS = 5;
//        public static string HARMONIC = "FO";


//        //[KW] Constants for the tuner configuration list view
//        public const int LV_FPGA = 1;
//        public const int LV_UPDOWNMODULE = 2;
//        public const int LV_LOSOURCE = 3;

       
//        #region Static Functions
        
//        public static SignalGeneratorDriver ConnectSignalGenerator(string signalGeneratorInstrStr)
//        {
//            SignalGeneratorDriver signalGenerator = null;
//            string[] connectionParams = signalGeneratorInstrStr.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

//            try
//            {
//                signalGenerator = (SignalGeneratorDriver) InternalSettings.CreateDriverInstance(connectionParams[0]);
//                signalGenerator.setVISAConnectionStr(connectionParams[1]);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Failed to create an instance of the signal generator due to an error. Check the connection string.", ex);
//            }

//            return signalGenerator;
//        }

//        public static PowerMeterDriver ConnectPowerMeter(string powerMeterDriverInstrStr)
//        {
//            PowerMeterDriver powerMeter = null;
//            string[] connectionParams = powerMeterDriverInstrStr.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

//            try
//            {
//                powerMeter = (PowerMeterDriver)InternalSettings.CreateDriverInstance(connectionParams[0]);
//                powerMeter.setVISAConnectionStr(connectionParams[1]);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Failed to create an instance of the power meter due to an error. Check the connection string.", ex);
//            }

//            return powerMeter;
//        }
//        #endregion


//        internal static SignalGeneratorDriver ConnectSignalGenerator(global::RLPCalibrationApplication.Classes.Instruments.InstrumentDataHolder _instrumentSetupData, string signalGeneratorInstrStr)
//        {
//            throw new NotImplementedException();
//        }

//        internal static PowerMeterDriver ConnectPowerMeter(global::RLPCalibrationApplication.Classes.Instruments.InstrumentDataHolder _instrumentSetupData, string powerMeterDriverInstrStr)
//        {
//            throw new NotImplementedException();
//        }


//    }
//}
