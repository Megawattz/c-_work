﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rapid.Utilities
{
    public enum PresetImpedanceGrids
    {
        Low = 0,
        Medium = 1,
        High = 2,
        Custom = 3
    }


    public enum InstrumentTypes
    {
        PowerMeter = 0,
        SignalGenerator = 1,
        RLP_Tuner = 2,
        VNA = 3,
        DCSupply = 4,
        RLP_Harmonic_Tuner = 5,
    }

    public enum DriverTypes
    {
        FOCUSCOM = 0,
        INTERNALnet = 1,
        EXTERNALnet = 2,
    }

    public enum VNAPortNumbers
    {
        PORT1 = 1,
        PORT2 = 2,
        PORT3 = 3,
        PORT4 = 4
    }

}
