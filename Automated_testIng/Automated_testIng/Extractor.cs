﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automated_testIng
{
    /// <summary>
    /// takes in a list and a accepteable range of power output and returns the frequency that is closests to the middle of that range 
    /// </summary>
    class Extractor
    {
        List<Tuple<double,double>> fullset;
        List<double> lowSet;
        List<double> highSet;
        List<double> withinTolerance;
        double upperRange;
        double lowerRange;
        double aim;

        //constuctor 
        public Extractor(List<Tuple<double, double>> listInput, double UpperInput, double LowerInput, double target)
        {
            fullset = listInput;
            upperRange = UpperInput;
            lowerRange = LowerInput;
            highSet = new List<double>();
            lowSet = new List<double>();
            withinTolerance = new List<double>();
            aim = target;
        }
        /// <summary>
        /// loops though list of values and returns the lowest of them
        /// </summary>
        /// <returns>lowest double</returns>
        double sortHigh()
        {
            double temp = 0;
            foreach (double i in highSet)
            {
                if (i < temp)
                {
                    temp = i;
                }
            }
            return temp;
        }
        /// <summary>
        /// loops though list of values and returns the highest of them
        /// </summary>
        /// <returns>highest double</returns>
        double sortLow()
        {
            double temp = 0;
            foreach (double i in lowSet)
            {
                if (i < temp)
                {
                    temp = i;
                }
            }
            return temp;
        }
        /// <summary>
        /// returns the result and make sure 0 isnt returned
        /// </summary>
        /// <param name="H">closeset value to the aim that it over it</param>
        /// <param name="L">closeset value to the aim that is under it</param>
        /// <param name="aim">the value we are aiming for</param>
        /// <returns></returns>
        double results(double H, double L)
        {
            if (highSet.Count() == 0 && lowSet.Count() != 0)
                return L;
            else if (lowSet.Count() == 0 && highSet.Count() != 0)
                return H;
            else if (lowSet.Count() != 0 && highSet.Count() != 0)
            {
                if (H - aim < L + aim)
                    return L;
                else
                    return H;
            }
            else return 100;
        }
        /// <summary>
        /// splits the list into 2 lists that both get processed into one value. those are compaired and which ever of the 2 values is closer to the target value is returned
        /// </summary>
        /// <returns>closest power output to the target</returns>
        public double findIdeal()
        {
            Fill_High_Low();
            double H = sortHigh();
            double L = sortLow();
            //returns the lowest value if the values are the same then L is returned.
            return results(H,L);
        }
        /// <summary>
        /// creates 2 list of powers ones within tolerance and one with figures that aren't
        /// </summary>
        public void Set__Within_Tolerance()
        {
            foreach (Tuple<double,double> i in fullset)
                if (i.Item2 > lowerRange && i.Item2 < upperRange)
                    withinTolerance.Add(i.Item2);             
        }
        /// <summary>
        /// fillout out the highest and lowest figures using values within Tolerance;
        /// </summary>
        public void Fill_High_Low()
        {
            Set__Within_Tolerance();
            foreach (double i in withinTolerance)
            {
                //if the current value is higher than the target we have made then then we add it to the high list
                if (i > aim)
                    highSet.Add(i);

                //if the current value is lower than the target range then add it to the lower list
                else if (i < aim)
                    lowSet.Add(i);
            }
        }
        /// <summary>
        /// returns a list thats within tolerance
        /// </summary>
        /// <returns>double list</returns>
        public List<double> get_Within_tolerance() { return withinTolerance; }
        
    }
}

