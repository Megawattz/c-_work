﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automated_testIng
{
    class basic_sorter : ISorter
    {
        public basic_sorter(List<Tuple<double,double>> input)
        {
            Starting_Values = input;
            end_values = new List<string>();

            ProcessList();
        }
        public List<string> end_values { get; set; }

        public List<Tuple<double, double>> Starting_Values { get; set; }

        public void ProcessList()
        {
            foreach ( Tuple<double,double>  i in Starting_Values)
            {
                end_values.Add(i.Item1.ToString() +  ", " +i.Item2.ToString());
            }
        }

        public List<string> returnList()
        {
            return end_values;
        }
    }
}
