﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Linq;

namespace Automated_testIng
{
    class XMLIO
    {
       

        public static string SerializeToString<T>(T objectToSerialize)
        {
            try
            {
                StringWriter sw = new Utf8StringWriter();
                XmlSerializer serializer = new XmlSerializer(typeof(T)); //, typeof(T).GetNestedTypes());
                serializer.Serialize(sw, objectToSerialize);

                return sw.ToString();

                //var writer = new StringWriter();
                //var serializer = new XmlSerializer((objectToSerialize.GetType()));
                //serializer.Serialize(writer, objectToSerialize);
                //string xml = writer.ToString();
                //return xml;

            }
            catch
            {
                throw;
            }
        }

        // It is important to override the XML string writer to UTF-8 for better compatiablity with xmlDocument
        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        public static T DeSerializeFromString<T>(string data)
        {
            T result;

            try
            {

                XmlSerializer serializer = new XmlSerializer(typeof(T)); //, typeof(T).GetNestedTypes());

                using (TextReader reader = new StringReader(data))
                {
                    result = (T)serializer.Deserialize(reader);
                }
            }
            catch
            {
                throw;
            }

            return result;
        }



    }
}
