﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Automated_testIng;

namespace Automated_testIng
{
    public partial class LandingForm : Form
    {
        Test_Properties test_Properties;
        Test_interpreter _Interpreter;
        Frequency_Sweep sweep;
        Find_Ideal_Power_Test power_Test;
        public LandingForm()
        {
            InitializeComponent();
            //create blank Test_Properties when the code starts
            test_Properties = new Test_Properties();
            _Interpreter = new Test_interpreter();
        }
        private void LoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog Load_in = new OpenFileDialog();
            if (Load_in.ShowDialog() == DialogResult.OK)
            {
                string Intake = System.IO.File.ReadAllText(Load_in.FileName);
                _Interpreter = XMLIO.DeSerializeFromString<Test_interpreter>(Intake);
                switch (_Interpreter.test_ID)
                {
                    case "Frequency_Sweep":
                            sweep = new Frequency_Sweep(_Interpreter.test_Properties);
                            sweep.Show();
                            this.Hide();
                            break;

                    case "Find_Ideal_Power_Test":
                        power_Test = new Find_Ideal_Power_Test(_Interpreter.test_Properties);
                        power_Test.Show();
                        this.Hide();
                        break;

                    default:
                            MessageBox.Show("file currupted cannot read");
                        break;
                        
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            sweep = new Frequency_Sweep(" ");
            sweep.Show();
            this.Hide();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            power_Test = new Find_Ideal_Power_Test(" ");
            power_Test.Show();
            this.Hide();
        }
    }
}
