﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rapid.Instruments.SignalGenerators
{
    public interface ILocalOscillatorSource
    {
        void setConnectionString(string connectionString = null,int chNum=0);
        void setFrequencyGHz(double frequency);
        void setPowerLevel(double powerLevel);
        void setRFStatus(bool rfStatus);
        void closeConnection();
        void setLOSource(double loFrequencyGHz, double powerLevel = 10, bool rfStatus = true, bool isInternalRef = false);
        void set10MHzReference(bool isInternal = true);

        double getFrequencyGHz();
        double getPowerLevel();
        bool getRFStatus();
        bool isConnectionValid();
    }
}
