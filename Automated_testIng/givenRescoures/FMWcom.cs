﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RLPCalibrationApplication.Classes.Instruments;
using Rapid.Utilities;

namespace RapidUtilities
{
    public static class FMWcom
    {

       // public FMWcom() { }
        private static  Dictionary<InstrumentTypes, string> DriverStrings;

        public static bool GetInstrumentCatagories()
        {
            DriverStrings = new Dictionary<InstrumentTypes, string>();

            FMWInstrument.IDriversCategories c = new FMWInstrument.DriversCategories();
            Dictionary<InstrumentTypes, string> _instrumentFriendlyNames = InstrumentHelper.FriendlyNames;

            foreach (var item in _instrumentFriendlyNames)
            {
                if(item.Key == InstrumentTypes.RLP_Harmonic_Tuner){}
                else if(item.Key== InstrumentTypes.RLP_Tuner){}
                else if (item.Key == InstrumentTypes.VNA) { }
                else
                {
                    string res = c.GetSupportedDriver(item.Value);

                    DriverStrings.Add(item.Key, res);
                }
            }

            return true;
        }

        public static string[] GetDriverStrings(InstrumentTypes inst) 
        {
            if(!DriverStrings.ContainsKey(inst)) return new string[0];

            string[] stringTypes = DriverStrings[inst].Split(new string[] { "\n" },
            StringSplitOptions.RemoveEmptyEntries);

            return stringTypes;
        }
    }

    public static class InternalDriverRegister
    {
        private static Dictionary<InstrumentTypes, List<string>> DriverStrings;

        public static bool GetInstrumentCatagories()
        {
            DriverStrings = new Dictionary<InstrumentTypes, List<string>>();

            List<string> SigGen =new List<string>(); 
            SigGen.Add("InternalRapid-v1001");

            DriverStrings.Add(InstrumentTypes.SignalGenerator, SigGen);

            return true;
        }

        public static string[] GetDriverStrings(InstrumentTypes inst)
        {
            if (!DriverStrings.ContainsKey(inst)) return new string[0];

            string[] stringTypes = DriverStrings[inst].ToArray();

            return stringTypes;
        }
    }
}
