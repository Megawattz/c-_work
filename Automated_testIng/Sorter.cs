﻿using System;
using System.Collections.Generic;

namespace Automated_testIng
{
    class Sorter : ISorter
    {
        double target;
        double upperlimit;
        double lowerlimit;
        double tolerance;
        public List<Tuple<double, double>> Starting_Values { get; set; }
        public List<string> end_values { get;  set; }

        //constructor
        public Sorter(double _tolerance, double _target,List<Tuple<double, double>> MainList)
        {
            upperlimit = 0;
            lowerlimit = 0;
            target = _target;
            tolerance = _tolerance;
            setLimits(target, tolerance);     
            Starting_Values = MainList;
            end_values = new List<string>();



            ProcessList();
        }

        //sets the upper and lower limits
       void setLimits(double aim, double tolerance)
        {
            upperlimit = aim + tolerance;
            lowerlimit = aim - tolerance;
        }

        //sorts the values fed in into good and bad lists 
       public void ProcessList()
        {
            //use turple as it can allows a 1 dimetion list to hold and pass multiple values
            foreach (Tuple<double,double> i in Starting_Values)
            {
                
                if (i.Item2 > lowerlimit && i.Item2 < upperlimit)
                {
                    end_values.Add((i.Item1 + ", " + i.Item2 +", " + "true"));
                }
                else
                    end_values.Add((i.Item1 + ", " + i.Item2 + ", " + "false"));
            }
        }
        //returns the results list
        public List<string> returnList()
        {
            return end_values;
        }

    }
}
