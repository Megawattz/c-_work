﻿namespace Automated_testIng
{
    partial class wait_dialogcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(wait_dialogcs));
            this.wait_bar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // wait_bar
            // 
            this.wait_bar.BackColor = System.Drawing.Color.White;
            this.wait_bar.Location = new System.Drawing.Point(42, 148);
            this.wait_bar.Name = "wait_bar";
            this.wait_bar.Size = new System.Drawing.Size(290, 20);
            this.wait_bar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.wait_bar.TabIndex = 1;
            // 
            // wait_dialogcs
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(360, 180);
            this.ControlBox = false;
            this.Controls.Add(this.wait_bar);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "wait_dialogcs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "wait_dialogcs";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ProgressBar wait_bar;
    }
}