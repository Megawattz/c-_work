﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerMeter;

namespace Automated_testIng
{
    class Dummy_PowerMeter : IPowerMeterDriver
    {
        //to be set by takeIn
        double _dBm;

        //to be set by takeIn
        double _GHz;

        //to be set by takeIN
        bool RfTrue;

        public void configure(int channel, double freqGHz)
        {
            
        }

        public void OPCWait(int timeOut_ms)
        {
            
        }

        //one to be worked on
        public double readPower(int channel)
        {
            //System.Threading.Thread.Sleep(100);
            TakeIN();

            //after taking in values we used them to calulate the output
            return _dBm - Math.Pow(_dBm, 2) * 0.01 - 3 - Math.Pow(_GHz, 2) * 0.01;
        }

        public void setAddress(int gpibAddress)
        {
            
        }

        public void setType(int channel, int type)
        {
            
        }

        public void setVISAConnectionStr(string VISAStr)
        {
            
        }

        //create a class to read in the text file that we have used to store data
        void TakeIN()
        {

            //read results into array of strings 
            string[] lines = System.IO.File.ReadAllLines("../../FakeReadings.txt");

            //check to see if we are in the right document
            //set GHz 
            _GHz = double.Parse(lines[0]);
            //set bDm
            _dBm = double.Parse(lines[1]);
            //set RFstatus
            RfTrue = bool.Parse(lines[2]);
        }
    }
}
