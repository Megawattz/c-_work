﻿namespace Automated_testIng
{
    partial class Config_SG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Config_SG));
            this.Done_Button = new System.Windows.Forms.Button();
            this.Cancel_Button = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ConfigBox = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.input_list = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Registry_read = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.ConfigBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Done_Button
            // 
            this.Done_Button.BackColor = System.Drawing.Color.SlateGray;
            this.Done_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Done_Button.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Done_Button.ForeColor = System.Drawing.Color.White;
            this.Done_Button.Location = new System.Drawing.Point(135, 0);
            this.Done_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Done_Button.Name = "Done_Button";
            this.Done_Button.Size = new System.Drawing.Size(136, 42);
            this.Done_Button.TabIndex = 0;
            this.Done_Button.Text = "Save";
            this.Done_Button.UseVisualStyleBackColor = false;
            this.Done_Button.Click += new System.EventHandler(this.Finishe_button_clicked);
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.BackColor = System.Drawing.Color.SlateGray;
            this.Cancel_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancel_Button.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Cancel_Button.ForeColor = System.Drawing.Color.White;
            this.Cancel_Button.Location = new System.Drawing.Point(0, 0);
            this.Cancel_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(136, 42);
            this.Cancel_Button.TabIndex = 0;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = false;
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Cancel_Button);
            this.panel1.Controls.Add(this.Done_Button);
            this.panel1.Location = new System.Drawing.Point(369, 319);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 45);
            this.panel1.TabIndex = 1;
            // 
            // ConfigBox
            // 
            this.ConfigBox.Controls.Add(this.textBox1);
            this.ConfigBox.Controls.Add(this.label3);
            this.ConfigBox.Controls.Add(this.label2);
            this.ConfigBox.Controls.Add(this.input_list);
            this.ConfigBox.Location = new System.Drawing.Point(12, 53);
            this.ConfigBox.Name = "ConfigBox";
            this.ConfigBox.Size = new System.Drawing.Size(627, 266);
            this.ConfigBox.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(285, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(339, 24);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Connection String Used";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Signal Generator Used";
            // 
            // input_list
            // 
            this.input_list.BackColor = System.Drawing.Color.White;
            this.input_list.CausesValidation = false;
            this.input_list.ForeColor = System.Drawing.Color.Black;
            this.input_list.FormattingEnabled = true;
            this.input_list.Location = new System.Drawing.Point(2, 25);
            this.input_list.Name = "input_list";
            this.input_list.Size = new System.Drawing.Size(277, 25);
            this.input_list.TabIndex = 0;
            this.input_list.SelectedValueChanged += new System.EventHandler(this.input_list_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(407, 33);
            this.label1.TabIndex = 3;
            this.label1.Text = "SignalGenerator Comfig Form";
            // 
            // Registry_read
            // 
            this.Registry_read.WorkerReportsProgress = true;
            this.Registry_read.WorkerSupportsCancellation = true;
            // 
            // Config_SG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(654, 371);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ConfigBox);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "Config_SG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configure Local Oscillator";
            this.panel1.ResumeLayout(false);
            this.ConfigBox.ResumeLayout(false);
            this.ConfigBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Done_Button;
        private System.Windows.Forms.Button Cancel_Button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel ConfigBox;
        private System.Windows.Forms.ComboBox input_list;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.ComponentModel.BackgroundWorker Registry_read;
    }
}