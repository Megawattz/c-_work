﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RapidUtilities;
using Rapid.Utilities;
using RLPCalibrationApplication.Classes.Instruments;

namespace Automated_testIng
{
    public partial class Config_SG : Form
    {
        private string currentString;
        private string currentInstrument;
        List<string> AvaibleDrivers;

        public InstrumentInfo SigGenInfo
        {
            get
            {
                return new InstrumentInfo() { ConnectionString = this.currentString, DriverName = currentInstrument, DriverType = DriverTypes.FOCUSCOM.ToString() };
            }
        }

        public Config_SG()
        {
            InitializeComponent();
            currentString = " ";
            currentInstrument = "";
            AvaibleDrivers = new List<string>();
            AvaibleDrivers = FMWcom.GetDriverStrings(InstrumentTypes.SignalGenerator).ToList<string>();
            input_list.DataSource = AvaibleDrivers;
        }

        private void Finishe_button_clicked(object sender, EventArgs e)
        {
            currentInstrument = input_list.SelectedItem.ToString();
            currentString = textBox1.Text;
            DialogResult = DialogResult.Yes;
        }
        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        /// <summary>
        /// updates currentinsterment every time the selected option changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void input_list_SelectedValueChanged(object sender, EventArgs e)
        {
            currentInstrument = input_list.SelectedItem.ToString();
        }
        /// <summary>
        /// evey time the text in the text box changes update the currentString variable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            currentString = textBox1.Text;
        }

    
    }
}
