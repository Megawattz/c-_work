﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RapidUtilities;
using Rapid.Utilities;
using RLPCalibrationApplication.Classes.Instruments;

namespace Automated_testIng
{
    public class MyTestSetup
    {
       public double Start_GHz;//for start of loop    
       public double End_GHz;//for end of loop
       public double Step_GHz;// for how much to increment loop by
       public double Power_dBm;// passing power though 
       public double Tolerance;//sets the tolerance of the output that it is acceptable 
       public  double Aim;//the target power output we would like 
       public double loopNumber;
       public InstrumentInfo SigGenInfo;
       public InstrumentInfo Pow_Met_Info;

    }
}
