﻿using Caliburn.Micro;
using MVVM_Core_Shapes.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Controls;

namespace MVVM_Core_Shapes.ViewModels
{
    public class StartUpPageViewModel : Screen
    {
        #region vars
        private Canvas _ExampleCanvas = new Canvas();
        public ObservableCollection<CircleModel> Circles { get; set; }
        #endregion

        public Canvas ExampleCanvas 
        {
        
        }

        #region constructor
        public StartUpPageViewModel()
        {
            Circles = new ObservableCollection<CircleModel>();
            Circles.Add(new CircleModel { X = 50, Y = 50, width = 50, Height = 50 });
            Circles.Add(new CircleModel { X = 100, Y = 100, width = 50, Height = 50 });
        }
        #endregion
    }
}
