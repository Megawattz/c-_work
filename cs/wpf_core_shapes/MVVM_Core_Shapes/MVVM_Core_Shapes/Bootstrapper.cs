﻿using Caliburn.Micro;
using MVVM_Core_Shapes.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace MVVM_Core_Shapes
{
    class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<StartUpPageViewModel>();
        }

    }
}
