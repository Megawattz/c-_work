﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVM_Core_Shapes.Models
{
    /// <summary>
    /// used to store a circle
    /// </summary>
    public class CircleModel
    {
        public double X { get; set; }

        public double Y { get; set; }

        public int width { get; set; }

        public int Height { get; set; }
    }
}
