﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows;
using System.IO;

namespace WpfCoreApp.cs_classes
{
    public class Controller : INotifyPropertyChanged
    {
        #region vars 
        public double X;
        public double Y;  
        public Canvas Main_Canvas;
        private CreateShape shapemaker;
        public int selected_Shape_Num;

        //list of shapes the user has made
        public Shape selectedShape;

        //array of shapes that can be selected
        private string[] _Shape_Types;
        public string[] Shape_Types
        {
            get
            {
                return _Shape_Types;
            }
            set
            {
                if (_Shape_Types != value)
                {
                    _Shape_Types = value;
                    OnPropertyChanged();
                }
            }
        }

        //text for button
        private string _Button_Text;
        public string Button_Text
        {
            get { return _Button_Text; }
            set
            {
                if (_Button_Text != value)
                {
                    _Button_Text = value;
                    OnPropertyChanged();
                }
            }
        }

        //list of shapes
        public List<Shape> shapes;

        //timer for the engine
        public DispatcherTimer timer;

        #region event hander for property changed event
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string property_name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property_name));
        }
        #endregion
        #endregion
        #region constructor
        public Controller(Canvas Source)
        {
            Main_Canvas = Source;
            shapes = new List<Shape>();
            CreateTimer();
            Shape_Types = new string[] { "square", "circle" };
            selectedShape = null;
            Change_Add_Button_Txt("nothing");
            shapemaker = new CreateShape(this);
            X = 0;
            Y = 0;
            selected_Shape_Num = 1;
        }

        /// <summary>
        /// creates a timer for the system to use
        /// </summary>
        private void CreateTimer()
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(2);
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Main_Canvas.Children.Clear();
            if (selectedShape != null)
            {
                Point mouse = Mouse.GetPosition(Main_Canvas);
                X = mouse.X - Math.Floor(selectedShape.Width / 2);
                Y = mouse.Y - Math.Floor(selectedShape.Height / 2);

                Canvas.SetLeft(selectedShape, X);
                Canvas.SetTop(selectedShape, Y);
                Main_Canvas.Children.Add(selectedShape);
            }
        }
        #endregion
        /// <summary>
        /// changes the add buttons text to whatever string
        /// is passed though.
        /// </summary>
        /// <param name="input"></param>
        public void Change_Add_Button_Txt(string input)
        {
            Button_Text = input;
        }

        public void Create_Shape()
        {
            shapemaker.runCreator(Button_Text);
        }

        public void clear()
        {
            Main_Canvas.Children.Clear();
        }
    }
}
