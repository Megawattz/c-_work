﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfCoreApp.cs_classes
{
    public class CreateShape
    {
        //controller to send shapes too once they are created
        private Controller TC;
        public CreateShape(Controller RC)
        {
            TC = RC;
        }
        /// <summary>
        /// makes a shape and adds it to the controllers list based on string passed in
        /// </summary>
        /// <param name="shapetype"></param>
        public void runCreator(string shapetype)
        {
            try
            {
                switch (shapetype)
                {
                    case "circle":
                        makeCircle();
                        break;

                    case "square":
                        makeSquare();
                        break;

                    default:
                        {
                            throw new Exception();
                        }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Break in CreateShape.cs", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        /// <summary>
        /// creates a cirlce of 50, 50 size
        /// </summary>
        private void makeCircle()
        { 
            Ellipse circle = new Ellipse();
            circle.Width = 50;
            circle.Height = 50;
            circle.Fill = Brushes.Blue;
            circle.Stroke = Brushes.Black;
            TC.selectedShape = circle;
         }

        /// <summary>
        /// creates a sqaure for the user
        /// </summary>
        private void makeSquare()
        {

            Rectangle rectangle = new Rectangle();
            rectangle.Width = 50;
            rectangle.Height = 50;
            rectangle.Fill = Brushes.Blue;
            rectangle.Stroke = Brushes.Black;
            TC.selectedShape = rectangle;        
        }
    }
}
