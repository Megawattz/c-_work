﻿using Caliburn.Micro;
using MVVM_CORE.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace MVVM_CORE
{
    class Bootstrapper : BootstrapperBase
    {
        /// <summary>
        /// constructor
        /// </summary>
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}
