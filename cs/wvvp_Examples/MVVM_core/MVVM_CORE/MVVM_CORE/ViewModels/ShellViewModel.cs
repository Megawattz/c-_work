﻿using Caliburn.Micro;
using MVVM_CORE.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVVM_CORE.ViewModels
{
    public class ShellViewModel : Screen
    {
        #region vars
        #region private vars
        private string _Name;
        private string _SecondName;
        private string _FullName;
        private Uri _TesterImage;
        private Uri ImagePath;
        private string ImageTag;


        private BindableCollection<PersonModel> _people = new BindableCollection<PersonModel>();
        private PersonModel _selectedPerson;
        #endregion
        #region binding Logic
        public string Name
        {
            get => _Name;
            set
            {
                _Name = value;
                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => FullName);
            }
        }
        public string SecondName
        {
            get => _SecondName;
            set
            {
                _SecondName = value;
                NotifyOfPropertyChange(() => SecondName);
            }
        }
        public string FullName
        {
            get { return _FullName; }
            set
            {
                _FullName = value;
                NotifyOfPropertyChange(() => FullName);
                if (_FullName != null)
                {
                    ImageTag = _FullName;
                }
            }
        }
        public BindableCollection<PersonModel> people
        {
            get { return _people; }
            set { _people = value; }
        }
        public PersonModel SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                _selectedPerson = value;
                NotifyOfPropertyChange(() => SelectedPerson);
                if (_selectedPerson != null)
                {
                    Name = _selectedPerson.FirstName;
                    SecondName = _selectedPerson.SecondName;
                    FullName = _selectedPerson.FullName;
                    TesterImage = changeImage();
                }
            }
        }
        public Uri TesterImage
        {
            get => _TesterImage;
            set
            {
                _TesterImage = value;
                NotifyOfPropertyChange(() => TesterImage);
            }
        }

        #endregion
        #endregion
        #region constructor
        public ShellViewModel()
        {
            people.Add(new PersonModel { FirstName = "Jonny", SecondName = "Davies" });
            people.Add(new PersonModel { FirstName = "Dave", SecondName = "Everyman" });
            people.Add(new PersonModel { FirstName = "Paul", SecondName = "Grey" });
            people.Add(new PersonModel { FirstName = "Jen", SecondName = "Powell" });
        }
        #endregion
        #region functions

        /// <summary>
        /// clears text from text boxes
        /// </summary>
        public void ClearText(string name, string secondName, string fullName)
        {
            Name = " ";
            SecondName = " ";
            FullName = " ";
        }

        /// <summary>
        /// returns bool for clear text button to tell if usable or not
        /// </summary>
        /// <param name="name"></param>
        /// <param name="secondName"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public bool CanClearText(string name, string secondName, string fullName)
        {
            return !String.IsNullOrWhiteSpace(Name) || !String.IsNullOrWhiteSpace(SecondName);
        }

        private Uri changeImage()
        {
            StringBuilder sb = new StringBuilder();
            //add all units in a string that are letters into a new string
            foreach (char C in FullName)
            {
                if (Char.IsLetter(C))
                {
                    sb.Append(C);
                }
            }
            sb.Append(".jfif");
            ImageTag = sb.ToString();
            string Path = "C:\\Users/jwatson/OneDrive - CyDen Limited/Backup/c-_work/cs/wvvp_Examples/MVVM_core/MVVM_CORE/MVVM_CORE/Images/" + ImageTag;
            Uri DebugUri = new Uri(Path);
            return DebugUri;
            
        }
        #endregion
    }
}
