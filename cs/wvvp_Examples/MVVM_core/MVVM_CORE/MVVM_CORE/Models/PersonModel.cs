﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVM_CORE.Models
{
    public class PersonModel
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FullName { get { return FirstName + " " + SecondName; } }
    }
}
