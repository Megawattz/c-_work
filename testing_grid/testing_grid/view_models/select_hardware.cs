﻿

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace testing_grid.xaml.Diagram
{
    public partial class select_hardware : Form
    {
        #region variables
        List<string> _types;
        List<Exception> ex_list;
        private string _driver;
        private string _hardware;
        private string _connection_string;
        System.IO.DirectoryInfo Directory;
        public List<BitmapImage> images;
        List<string> paths;
        string _MainPath;

        //variables that requrire  being accessable from outside the dialog.
        public string MainPath
        {
            get { return _MainPath; }
            set { _MainPath = value; }
        }

        public string Driver
        {
            get { return _driver; }
            set { _driver = value; }
        }

        public string Hardware_type
        {
            get { return _hardware; }
            set { _hardware = value; }
        }

        public string Connection_string
        {
            get { return _connection_string; }
            set { _connection_string = value; }
        }
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public select_hardware()
        {
            InitializeComponent();

            // a list to return to the main wpf form to allow us to see if and where errors have occured in this form
            ex_list = new List<Exception>();

            //to export the images to the rest of the application for use in the board
            images = new List<BitmapImage>();

            //only run if the directory is blank when the form is loaded in
            if (Directory == null)
            {
                _types = FillList();
                paths = AddPaths();
                Hardware_Type_list.DataSource = _types;
            }
        }

        #region filling lists
        /// <summary>
        /// fills the list with options for selecting hardware
        /// </summary>
        /// <returns></returns>
        private List<string> FillList()
        {
            List<string> temp = new List<string>
            {
                "Power_Meter",
                "Signal_Generator",
                "Nosie_Meter",
                "other"
            };

            return temp;
        }

        /// <summary>
        /// fills a list with paths to images for the user to add
        /// </summary>
        /// <returns></returns>
        private List<string> AddPaths()
        {
            List<string> temp = new List<string>();
            #region readFolder
            //create  a bool to end do while 
            bool good = false;
            //dailog to select a file to use
            FolderBrowserDialog fBD = new FolderBrowserDialog();
            /*ive used a do while loop as we need to do this code at least once but we don't know how many
            *we will need the user to run it to get useful objects 
            */
            do
            {
                //show the dialog
                DialogResult R = fBD.ShowDialog();

                //if the dialog comes back with a result
                if (R == DialogResult.OK)
                {
                    _MainPath = fBD.SelectedPath.ToString();
                    //create the directory
                    Directory = new System.IO.DirectoryInfo(_MainPath);
                    //see if directory is good to use 
                    if (Directory.Exists)
                    {
                        try
                        {
                            //if it is get all the file that have a file extention of ".jpg"
                            FileInfo[] File_Info = Directory.GetFiles("*.jpg");
                            if (File_Info != null)
                            {
                                /*first because the file info list isn't empty we know we have images we can use
                                *then we can turn the bool that ends the while loop to stop it looping
                                */
                                good = true;
                                //for each type of hardware we have added in "fillList" function
                                foreach (string Title in _types)
                                {
                                    //merge the .jpg on the end so we can find the assoiaited image
                                    string testVal = string.Concat(Title, ".jpg");

                                    //then go though each of the files that was a .jpg in the folder space we was in
                                    foreach (var i in File_Info)
                                    {
                                        //if its tag matches the one we made above then add it to a list of image paths 
                                        if (i.Name == testVal)
                                        {
                                            temp.Add(i.FullName);

                                            BitmapImage BI = new BitmapImage(new Uri(i.FullName));
                                            images.Add(BI);
                                            //once we have it added there cannot be 2 so break the loop
                                            break;
                                        }
                                    }
                                }
                            }
                            //if the folder has no .jpg image there is an issue, it could the user or the iages themselfs
                            else
                            {
                                //so we throw an error to get caught of a null data type
                                throw new ArgumentNullException(nameof(Directory.Name), "No jpg files in the selected folder");
                            }
                        }
                        catch (Exception e)
                        {
                            //once caught show the user a message so they know somthings gone wrong and catalog the error
                            MessageBox.Show(e.Message + ". source " + e.Source);
                            ex_list.Add(e);
                        }
                    }
                }
                //end of do while loop
            } while (!good);
            #endregion
            //return the list of the paths to the variable that called the fucntion
            return temp;
        }

        #endregion

        /// <summary>
        /// finds out what button has been hit and returns the appriate action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Buttonhit(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            #region saveButton
            //if the buttons the save button we will close the form and make sure the strings that are to be used in the wpf user contorll is correct 
            if (b.Name == "but_save")
            {
                try
                {
                    Driver = selectDrivers_dropbox.SelectedItem.ToString();
                    Hardware_type = Hardware_Type_list.SelectedItem.ToString();
                    Connection_string = connectionSting_input.Text;
                    DialogResult = DialogResult.Yes;
                    Close();
                }
#pragma warning disable CS0168 // The variable 'x' is declared but never used
                catch (Exception x)
#pragma warning restore CS0168 // The variable 'x' is declared but never used
                {

                }
            }
            #endregion

            #region Reset Button
            //if the button clicked is the reset button then we will wipe the form clean
            else if (b.Name == "but_reset")
            {
                selectDrivers_dropbox.SelectedItem = "";
                Hardware_Type_list.SelectedItem = "";
                connectionSting_input.Text = "";
            }
            #endregion

            #region cancel button
            //else the button must be the cancel so close the form will dialog cancel
            else if (b.Name == "but_cancel")
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            #endregion
            #region reconfig 
            else
            //this will do the file choice the user made at the start of the form amd refill the list boxes to match
            {
                _types = FillList();
                paths = AddPaths();
                Update_image();
            }
            #endregion

        }

        /// <summary>
        /// runs update_image when type of hardware changes 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectDrivers_dropbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //check to make sure the box has a option in                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
            if (Hardware_Type_list.SelectedItem != null)
            {
                Hardware_type = Hardware_Type_list.SelectedValue.ToString();
            }
            else
            {
                Hardware_type = null;
            }
            Update_image();

        }

        /// <summary>
        /// uses a switch to change image in display 
        /// </summary>
        private void Update_image()
        {
            try
            {
                #region temp vars
                //create a string with the path to the folder we chose eailer so we know we are in the rigth file space
                string Temp_path = Directory.FullName;

                //index for finding the path we need from the paths 
                int Indx;

                //for making a bitmap list to use the main wpf form 
                BitmapImage tempstorage;
                #endregion

                //switch for each type of hardware to extract the rigth image.
                switch (Hardware_type)
                {
                    //first the match we are looking for 
                    case ("Power_Meter"):
                        {
                            //add the files tag onto the end of directory string 
                            Temp_path += "\\Power_Meter.jpg";
                            //find which element of the images paths is the right one for us 
                            Indx = paths.FindIndex(0, paths.Count, cl => cl == Temp_path);
                            //load the image into the imagebox
                            Preview_Image.Image = Image.FromFile(paths[Indx]);
                            //make sure the image if fitted into the space we have on the form
                            Preview_Image.SizeMode = PictureBoxSizeMode.StretchImage;

                            //create a full bitmapImage and add it to the list
                            tempstorage = new BitmapImage(new Uri(Temp_path));
                            images.Add(tempstorage);

                            //break the process as we have done all we need 
                            break;
                        }

                    //the rest of the cases are the same but the names of the files tags on the end have changed 
                    case ("Signal_Generator"):
                        {
                            Temp_path += "\\Signal_Generator.jpg";
                            Indx = paths.FindIndex(0, paths.Count, cl => cl == Temp_path);
                            BitmapImage SG = new BitmapImage(new Uri(Temp_path));
                            Preview_Image.Image = Image.FromFile(paths[Indx]);
                            Preview_Image.SizeMode = PictureBoxSizeMode.StretchImage;
                            //create a full bitmapImage and add it to the list
                            tempstorage = new BitmapImage(new Uri(Temp_path));
                            images.Add(tempstorage);

                            break;
                        }
                    case ("Nosie_Meter"):
                        {
                            Temp_path += "\\Nosie_Meter.jpg";
                            Indx = paths.FindIndex(0, paths.Count, cl => cl == Temp_path);
                            Preview_Image.Image = Image.FromFile(paths[Indx]);
                            Preview_Image.SizeMode = PictureBoxSizeMode.StretchImage;
                            //create a full bitmapImage and add it to the list
                            tempstorage = new BitmapImage(new Uri(Temp_path));
                            images.Add(tempstorage);

                            break;
                        }


                    //and if the thing we are looking for isn't listed then go to the default image 
                    default:
                        Temp_path += "\\other.jpg";
                        Indx = paths.FindIndex(0, paths.Count, cl => cl == Temp_path);
                        Preview_Image.Image = Image.FromFile(paths[Indx]);
                        Preview_Image.SizeMode = PictureBoxSizeMode.StretchImage;
                        //create a full bitmapImage and add it to the list
                        tempstorage = new BitmapImage(new Uri(Temp_path));
                        images.Add(tempstorage);

                        break;
                }
            }
            //the 2 faults that could occur are the path not being in the stored paths list or a image not being useful
            catch (Exception ex)
            {
                //if an error does occur then catch it and tell the user now to fix it and capture the error in a list 
                MessageBox.Show(ex.Message + "please reconfigure");
            }
        }


    }
}

