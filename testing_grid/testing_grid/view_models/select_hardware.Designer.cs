﻿namespace testing_grid.xaml.Diagram
{
    partial class select_hardware
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(select_hardware));
            this.connectionSting_input = new System.Windows.Forms.TextBox();
            this.but_cancel = new System.Windows.Forms.Button();
            this.but_reset = new System.Windows.Forms.Button();
            this.selectDrivers_dropbox = new System.Windows.Forms.ComboBox();
            this.but_save = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Preview_Image = new System.Windows.Forms.PictureBox();
            this.Hardware_Type_list = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.but_reconfigure = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Preview_Image)).BeginInit();
            this.SuspendLayout();
            // 
            // connectionSting_input
            // 
            this.connectionSting_input.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.connectionSting_input.Location = new System.Drawing.Point(191, 92);
            this.connectionSting_input.Name = "connectionSting_input";
            this.connectionSting_input.Size = new System.Drawing.Size(338, 21);
            this.connectionSting_input.TabIndex = 0;
            // 
            // but_cancel
            // 
            this.but_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.but_cancel.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.but_cancel.Location = new System.Drawing.Point(418, 240);
            this.but_cancel.Name = "but_cancel";
            this.but_cancel.Size = new System.Drawing.Size(111, 36);
            this.but_cancel.TabIndex = 1;
            this.but_cancel.Text = "Cancel";
            this.but_cancel.UseVisualStyleBackColor = true;
            this.but_cancel.Click += new System.EventHandler(this.Buttonhit);
            // 
            // but_reset
            // 
            this.but_reset.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.but_reset.Location = new System.Drawing.Point(301, 240);
            this.but_reset.Name = "but_reset";
            this.but_reset.Size = new System.Drawing.Size(111, 36);
            this.but_reset.TabIndex = 1;
            this.but_reset.Text = "Reset";
            this.but_reset.UseVisualStyleBackColor = true;
            this.but_reset.Click += new System.EventHandler(this.Buttonhit);
            // 
            // selectDrivers_dropbox
            // 
            this.selectDrivers_dropbox.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.selectDrivers_dropbox.FormattingEnabled = true;
            this.selectDrivers_dropbox.Items.AddRange(new object[] {
            "placeHolder_1",
            "placeHolder_2"});
            this.selectDrivers_dropbox.Location = new System.Drawing.Point(191, 58);
            this.selectDrivers_dropbox.Name = "selectDrivers_dropbox";
            this.selectDrivers_dropbox.Size = new System.Drawing.Size(338, 24);
            this.selectDrivers_dropbox.TabIndex = 2;
            this.selectDrivers_dropbox.SelectedIndexChanged += new System.EventHandler(this.SelectDrivers_dropbox_SelectedIndexChanged);
            // 
            // but_save
            // 
            this.but_save.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.but_save.Location = new System.Drawing.Point(191, 240);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(104, 36);
            this.but_save.TabIndex = 3;
            this.but_save.Text = "save";
            this.but_save.UseVisualStyleBackColor = true;
            this.but_save.Click += new System.EventHandler(this.Buttonhit);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label1.Location = new System.Drawing.Point(146, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Driver";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label2.Location = new System.Drawing.Point(81, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Connection String";
            // 
            // Preview_Image
            // 
            this.Preview_Image.ErrorImage = null;
            this.Preview_Image.Location = new System.Drawing.Point(399, 119);
            this.Preview_Image.Name = "Preview_Image";
            this.Preview_Image.Size = new System.Drawing.Size(130, 120);
            this.Preview_Image.TabIndex = 5;
            this.Preview_Image.TabStop = false;
            // 
            // Hardware_Type_list
            // 
            this.Hardware_Type_list.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.Hardware_Type_list.FormattingEnabled = true;
            this.Hardware_Type_list.Items.AddRange(new object[] {
            "Power_Meter",
            "Signal_Generator",
            "other"});
            this.Hardware_Type_list.Location = new System.Drawing.Point(191, 28);
            this.Hardware_Type_list.Name = "Hardware_Type_list";
            this.Hardware_Type_list.Size = new System.Drawing.Size(338, 24);
            this.Hardware_Type_list.TabIndex = 2;
            this.Hardware_Type_list.SelectedIndexChanged += new System.EventHandler(this.SelectDrivers_dropbox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label3.Location = new System.Drawing.Point(146, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Type ";
            // 
            // but_reconfigure
            // 
            this.but_reconfigure.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.but_reconfigure.Location = new System.Drawing.Point(81, 240);
            this.but_reconfigure.Name = "but_reconfigure";
            this.but_reconfigure.Size = new System.Drawing.Size(104, 36);
            this.but_reconfigure.TabIndex = 3;
            this.but_reconfigure.Text = "reconfigure";
            this.but_reconfigure.UseVisualStyleBackColor = true;
            this.but_reconfigure.Click += new System.EventHandler(this.Buttonhit);
            // 
            // select_hardware
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 288);
            this.ControlBox = false;
            this.Controls.Add(this.Preview_Image);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_reconfigure);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.Hardware_Type_list);
            this.Controls.Add(this.selectDrivers_dropbox);
            this.Controls.Add(this.but_reset);
            this.Controls.Add(this.but_cancel);
            this.Controls.Add(this.connectionSting_input);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "select_hardware";
            this.Text = "select_hardware";
            ((System.ComponentModel.ISupportInitialize)(this.Preview_Image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox connectionSting_input;
        private System.Windows.Forms.Button but_cancel;
        private System.Windows.Forms.Button but_reset;
        private System.Windows.Forms.ComboBox selectDrivers_dropbox;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox Preview_Image;
        private System.Windows.Forms.ComboBox Hardware_Type_list;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button but_reconfigure;
    }
}