﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Xml.Serialization;
using testing_grid.Classes;
using testing_grid.Classes.command_pattern;
using testing_grid.Classes.UI;
using testing_grid.xaml.Diagram;
using testing_grid.xaml.Diagram.Classes;
using Image = System.Windows.Controls.Image;
using Point = System.Windows.Point;

namespace testing_grid
{
    /// <summary>
    /// used to give the side a route should connect to.
    /// </summary>
    public enum direction
    {
        right,
        left,
        top,
        bottom
    }
    /// <summary>
    /// Interaction logic for DataGrid.xaml
    /// </summary>
    /// 
    public partial class DataGrid : System.Windows.Controls.UserControl
    {
        //start of DataGrid Class
        #region variables
        //variables required for the form
        int CX;
        int CY;
        string path;
        string blank;
        public bool dev;

        Board Current_Diagram;
        Tile_Texture_Manger Grid_Textures;
        Tile_Texture_Manger textures;

        List<Exception> errors;
        #endregion
        /// <summary>
        /// construtor requires nothing to be passed though to the user control
        /// </summary>
        public DataGrid()
        {
            InitializeComponent();
            DataContext = this;
            dev = false;
        }
        /// <summary>
        /// returns the xy of the mouse on the main canvas at time of left mouse button click whilst on the canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeTile(object sender, MouseButtonEventArgs e)
        {
            Current_Diagram.Current_Tile = new Point2Int();
            //get the mouse positon in real in at point of clicking
            Point mousePos = e.GetPosition(MainDIsplay);
            Point2Int mouseTilePos = Get_Tile_From_Mouse(mousePos.X, mousePos.Y);
            Current_Diagram.Current_Tile = mouseTilePos;
            //check if the developer mode is enabled or not
            if (!dev)
            {
                //start the controller object
                Remote remote = new Remote(Current_Diagram);
                //create a developer board
                Developer_Board developer_Board = new Developer_Board();
                //send command baised on the contents of tile clicked on
                if (mouseTilePos.tileType != Current_Diagram.selected_hardware)
                {
                    remote.Invoke(developer_Board);
                }
                else
                {
                    remote.Undo(developer_Board);
                }
                drawDiagram();
            }
        }
        /// <summary>
        /// runs the setup windows for the xaml
        /// </summary>
        private void NewDiagram()
        {
            //get the display area we are working with in pixels
            CX = (int)Current_Diagram.Width;
            CY = (int)Current_Diagram.Height;

            /*devide the pixel height and width by the area taken by a tile
             *  to the get the x and y scale of the display we
             *  that we have to work with,
             *  then and store them
             */
            int Height = (CY / Tile_Texture_Manger.Tile_Size);
            int Width = (CX / Tile_Texture_Manger.Tile_Size);
            //make the current display show a canvas and grid of the correct size
            Current_Diagram.Resize(Width, Height, true);
            //redo the canvas to stop out of bounds area error
            Current_Diagram.Width = Width * Tile_Texture_Manger.Tile_Size;
            Current_Diagram.Height = Height * Tile_Texture_Manger.Tile_Size;

            MainDIsplay.Width = Width * Tile_Texture_Manger.Tile_Size;
            MainDIsplay.Height = Height * Tile_Texture_Manger.Tile_Size;

        }
        /// <summary>
        /// this function takes a image in and changes the source to the 
        /// directory you pass though to it.
        /// redraws all the images in the grid when called
        /// </summary>
        /// <param name="dir">the directory of the image we want to change it to</param>
        /// <param name="image"></param>
        /// <returns>the image with a new referance</returns>
        private void drawDiagram()
        {
            #region routing
            if (Current_Diagram.Hardware.Count >= 2)
            {
                foreach (Line Line in Current_Diagram.Routes)
                {
                    Routeing_Configuration _Configuration = new Routeing_Configuration(0, new Point2Int());
                    Line drawingLine = _Configuration.CreateLine((int)Line.X1, (int)Line.X2, (int)Line.Y1, (int)Line.Y2);
                }
            }


            MainDIsplay.Children.Clear();
            for (int i = 0; i < Current_Diagram.Routes.Count; i++)
            {
                MainDIsplay.Children.Add(Current_Diagram.Routes[i]);
            }
            #endregion
            for (int y = 0; y < (Current_Diagram.Height / Tile_Texture_Manger.Tile_Size); y++)
            {
                for (int x = 0; x < (Current_Diagram.Width / Tile_Texture_Manger.Tile_Size); x++)
                {
                    int index = ((Current_Diagram.Width / Tile_Texture_Manger.Tile_Size) * y) + x;

                    Point2Int looking_at = Current_Diagram.tiles[index];

                    if (looking_at.HeadTile == 2)
                    {
                        Image texture = new Image
                        {
                            Width = Tile_Texture_Manger.Tile_Size * looking_at.size,
                            Height = Tile_Texture_Manger.Tile_Size * looking_at.size,
                        };

                        Point2Int tilePosition = Tile_Utils.GetPixelFromTileCoordinates(new Point2Int(x, y));
                        Canvas.SetLeft(texture, tilePosition.X);
                        Canvas.SetTop(texture, tilePosition.Y);
                        bool hide = true;
                        switch (looking_at.tileType)
                        {
                            case eTileType.blank:
                                if (hide)
                                    texture.Source = null;
                                else
                                    texture.Source = textures.Blank_Icon;
                                break;

                            case eTileType.Bias_Tee:
                                texture.Source = textures.Bias_Tee;
                                break;

                            case eTileType.controller:
                                texture.Source = textures.Controller;
                                break;

                            case eTileType.DC_Power:
                                texture.Source = textures.DC_Power;
                                break;

                            case eTileType.Dut:
                                texture.Source = textures.Dut;
                                break;

                            case eTileType.Network_Analyzer:
                                texture.Source = textures.Network_Analyzer;
                                break;

                            case eTileType.Noise_Meter:
                                texture.Source = textures.Noise_Meter;
                                break;

                            case eTileType.Rf_Source:
                                texture.Source = textures.RF_Source;
                                break;

                            case eTileType.Switch:
                                texture.Source = textures.Switch;
                                break;

                            case eTileType.Tuner:
                                texture.Source = textures.Tuner;
                                break;

                            default:
                                break;
                        }

                        MainDIsplay.Children.Add(texture);
                    }
                }
            }

        }
        /// <summary>
        /// opens a file dialog and loads in the images the user selects
        /// </summary>
        private void getDirectory()
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

            string tempPath;
            DialogResult R = folderBrowser.ShowDialog();

            if (R == DialogResult.OK)
            {
                //get the dialog path as a string 
                tempPath = folderBrowser.SelectedPath.ToString();

                //create a file paser 
                textures = new Tile_Texture_Manger();

                //use it to pass textures though to paser
                Grid_Textures = Paser.ImportTextures(textures, tempPath);
            }
        }
        /// <summary>
        /// when used gives us the area we clicked on and the infomation of the area. if hardware we get the details
        /// and fill the combobox on the display with the names of the hardware we can connect to.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainDIsplay_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            comboBox.ItemsSource = null;
            //for the combobox later
            List<string> nameOfHardware = new List<string>();
            //get the area on the screen we clicked on in pixels
            Point mouse = e.GetPosition(MainDIsplay);

            Current_Diagram.Current_Tile = Get_Tile_From_Mouse(mouse.X, mouse.Y);

            //see if the tile we are on is hardware and in the grid
            if (Current_Diagram.Current_Tile.tileType != eTileType.blank)
            {
                //remove the selected tile from the list
                Current_Diagram.Hardware.Remove(Current_Diagram.Current_Tile);

                //make a suiatble list of labels of the hardware for the user
                for (int i = 0; i < Current_Diagram.Hardware.Count; i++)
                {
                    nameOfHardware.Add(Current_Diagram.Hardware[i].Name);
                }
                //do the combo box
                comboBox.ItemsSource = nameOfHardware;
                //readd the seleted tile back to hardware referance list
                Current_Diagram.Hardware = Check_Current_Hardware();
            }
            else
            {
                comboBox.ItemsSource = null;
            }
            currenty_seleted_Label.Content = Current_Diagram.Current_Tile.Name;
        }
        /// <summary>
        /// finds the 2 points on 2 hardware seleted for routing and stores the line without rendering 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Creating_lines_Click(object sender, RoutedEventArgs e)
        {
            Remote remote = new Remote(Current_Diagram);
            Routing_Command routing_Command = new Routing_Command();
            remote.Invoke(routing_Command);
            drawDiagram();
        }
        /// <summary>
        /// takes the same process as creating a line but removes the route if it's there instead
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Remove_lines_but_Click(object sender, RoutedEventArgs e)
        {
            Remote remote = new Remote(Current_Diagram);
            Routing_Command routing_Command = new Routing_Command();
            remote.Undo(routing_Command);
            drawDiagram();
        }
        /// <summary>
        /// transfers key infomation to the save class and serilies it 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void save_But(object sender, RoutedEventArgs e)
        {
            //clear the route infomation
            Current_Diagram.Route_Info.Clear();

            saveClass save = new saveClass();
            foreach (Line line in Current_Diagram.Routes)
            {
                Current_Diagram.Route_Info.Add(line.X1 + "," + line.Y1 + "," + line.X2 + "," + line.Y2);
            }

            SaveFileDialog saveFile = new SaveFileDialog();

            saveFile.FileName = "diagram_Save";
            saveFile.DefaultExt = "XML";
            saveFile.Filter = "(.XML)| .XML";

            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                save.HardwareList = Current_Diagram.Hardware;
                save.Route_Info = Current_Diagram.Route_Info;
                save._Tiles = Current_Diagram.tiles;

                string savepath = saveFile.FileName;
                string diagram = XML_Saver.SerializeObject<saveClass>(save);

                using (StreamWriter sw = new StreamWriter(savepath))
                {
                    sw.WriteLine(diagram);
                }
            }
        }
        /// <summary>
        /// reads in a xml file using a custom xml reader and then makes the board and hardware match 
        /// the list of hardware saved down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restore_Diagram(object sender, RoutedEventArgs e)
        {
            comboBox.ItemsSource = null;
            Current_Diagram = new Board((int)MainDIsplay.Height, (int)MainDIsplay.Width);
            Current_Diagram.Routes = new List<Line>();
            Current_Diagram.Hardware = new List<Point2Int>();


            saveClass load = new saveClass();

            OpenFileDialog fileDialog = new OpenFileDialog();

            fileDialog.Filter = "(*.XML) | *.XML";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (var reader = new StreamReader(fileDialog.FileName))
                    {
                        XmlSerializer deserializer = new XmlSerializer(typeof(saveClass), new XmlRootAttribute("saveClass"));
                        load = (saveClass)deserializer.Deserialize(reader);
                    }
                }
                catch (Exception E)
                {
                    System.Windows.MessageBox.Show("Error cannot read in file" + "\n" + " " + E.Message);
                }
                Current_Diagram.Hardware = load.HardwareList;
                Current_Diagram.tiles = load._Tiles;


                NewDiagram();
                Current_Diagram.Hardware = Check_Current_Hardware();
                restore_Routes(load.Route_Info);
                //takes the restored routes and adds the route infomation into the save route
                foreach (Line line in Current_Diagram.Routes)
                {
                    Current_Diagram.Route_Info.Add(line.X1 + "," + line.Y1 + "," + line.X2 + "," + line.Y2);
                }
                drawDiagram();
            }
        }
        /// <summary>
        /// takes apart a  storage string and makes a line list out of the string
        /// </summary>
        private void restore_Routes(List<string> string_format_Routes)
        {
            foreach (string route in string_format_Routes)
            {
                List<int> Input_route = new List<int>();
                char spilter = ',';
                List<string> i = route.Split(spilter).ToList<string>();

                for (int x = 0; x < i.Count; x++)
                {
                    int tempInt;
                    if (int.TryParse(i[x], out tempInt))
                    {
                        Input_route.Add(tempInt);
                    }
                }
                if (Input_route.Count != 0)
                {
                    Routeing_Configuration R = new Routeing_Configuration(0, new Point2Int());
                    Line routeHolder = R.CreateLine(Input_route[0], Input_route[1], Input_route[2], Input_route[3]);
                    Current_Diagram.Routes.Add(routeHolder);
                    Current_Diagram.Route_Info.Add(routeHolder.ToString() + "," + routeHolder.ToString() + "," + routeHolder.ToString() + "," + routeHolder.ToString() + "," + routeHolder.Stroke.ToString());
                }
            }
        }
        /// <summary>
        /// on loaded fucntion that acts as a constructor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dave_Loaded(object sender, RoutedEventArgs e)
        {
            #region blank vars

            //blank var
            blank = "blank";
            Current_Diagram = new Board((int)MainDIsplay.Height, (int)MainDIsplay.Width);
            #endregion
            //get the images loaded into memory
            getDirectory();
            //run the builder for the diagram
            NewDiagram();
            List<eTileType> tileTypes = new List<eTileType>();
            try
            {
                for (int i = 0; i < textures.bitmaps.Count; i++)
                {

                    //adds text of hardware to the treeview
                    string item = textures.bitmaps[i];
                    HardwareTree.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }
        }
        /// <summary>
        /// takes new selected item in the listbox and converts the result into a tiletype enum
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HardwareTree_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = HardwareTree.SelectedItem.ToString();
            //convert the name to an emun we can use
            Enum.TryParse(name, true, out Current_Diagram.selected_hardware);
        }
        /// <summary>
        /// resets the board to a blank board
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_but_Click(object sender, RoutedEventArgs e)
        {
            comboBox.ItemsSource = null;

            Current_Diagram = new Board(Current_Diagram.Height, Current_Diagram.Width);
            Current_Diagram.Routes = new List<Line>();
            Current_Diagram.Route_Info = new List<string>();
            NewDiagram();
            drawDiagram();
        }
        /// <summary>
        /// returns the mouse postion as a tile in the canvas space
        /// </summary>
        /// <param name="Mouse_x"></param>
        /// <param name="Mouse_y"></param>
        /// <returns></returns>
        private Point2Int Get_Tile_From_Mouse(double Mouse_x, double Mouse_y)
        {
            //get the area we are clicking into a tilesize we can use to change the canvus as a tile grid
            Point2Int mouseTilePos = Tile_Utils.GetTileCoordinatesFromPixel(new Point2Int/*means as a interger*/((int)Mouse_x, /*means as a interger*/(int)Mouse_y));
            int index = ((Current_Diagram.Width / Tile_Texture_Manger.Tile_Size) * mouseTilePos.Y) + mouseTilePos.X;

            //get what the selected tile currently  is
            mouseTilePos = Current_Diagram.tiles[index];

            if (mouseTilePos.tileType != eTileType.blank)
            {

                int counter = mouseTilePos.Group.Count;

                for (int i = 0; i < counter; i++)
                {
                    Point2Int looking_At = Current_Diagram.tiles[mouseTilePos.Group[i]];
                    if (looking_At.HeadTile == 2)
                    {
                        return looking_At;
                    }
                }
            }
            return mouseTilePos;
        }
        /// <summary>
        /// goes though grid and fills out hardware list to be accurate to the current board
        /// </summary>
        /// <returns></returns>
        private List<Point2Int> Check_Current_Hardware()
        {
            //make the list of hardware blank
            List<Point2Int> new_hardware_list = new List<Point2Int>();

            //loop thought the entire board
            for (int i = 0; i < Current_Diagram.tiles.Count; i++)
            {
                //if the current tile being looked at is a head tile (to stop mutilple small images being rendered) and the tiles type isn't a blank tile 
                if ((Current_Diagram.tiles[i].tileType != eTileType.blank) && (Current_Diagram.tiles[i].HeadTile == 2))
                {
                    //add current tile to the hardwaree list
                    new_hardware_list.Add(Current_Diagram.tiles[i]);
                }
            }
            //return the new list
            return new_hardware_list;
        }
        /// <summary>
        /// To be accessed by controls 
        /// outside of the of the xmal
        /// to change the mode of the
        /// data grid to developer or customer
        /// </summary>
        public void change_developer_State()
        {
            //if the developer mode is false
            if (!dev)
            {
                //make the user controls that are there for the developer are made invisible
                Creating_lines_but.Visibility = Visibility.Hidden;
                Remove_lines_but.Visibility = Visibility.Hidden;
                save_but.Visibility = Visibility.Hidden;
                reset_but.Visibility = Visibility.Hidden;
                HardwareTree.Visibility = Visibility.Hidden;
                comboBox.Visibility = Visibility.Hidden;
            }
            else
            {
                //makes the user controls that are there for developers are made visible
                Creating_lines_but.Visibility = Visibility.Visible;
                Remove_lines_but.Visibility = Visibility.Visible;
                save_but.Visibility = Visibility.Visible;
                reset_but.Visibility = Visibility.Visible;
                HardwareTree.Visibility = Visibility.Visible;
                comboBox.Visibility = Visibility.Visible;
            }
            //flip the bool value controlling this functions appilation 
            dev = change_bool(dev);
        }
        /// <summary>
        ///Takes a bool and flips its value
        /// </summary>
        /// <param name="current_state"></param>
        /// <returns></returns>
        private bool change_bool(bool current_state)
        {
            //if not true set to true
            if (!current_state)
            {
                current_state = true;
            }
            else//if this code is hit the bool must be true and therefore we set it to false
            {
                current_state = false;
            }
            //return the bool after alteration
            return current_state;
        }
        /// <summary>
        /// updates stored value of selected hardware used
        /// as the target for the route create command 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox.SelectedItem != null)
            {
                Current_Diagram.ComboBox_Selected_Value = comboBox.SelectedItem.ToString();
            }
            else
            {
                Current_Diagram.ComboBox_Selected_Value = "";
            }
        }
        //update the target
    }
}


