﻿using testing_grid.xaml.Diagram.Classes;

namespace testing_grid.Classes
{
    public interface ICommand
    {
        void Execute(Board _board);
        void Reverse(Board _board);
    }
}
