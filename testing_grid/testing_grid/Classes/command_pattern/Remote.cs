﻿using testing_grid.xaml.Diagram.Classes;

namespace testing_grid.Classes.command_pattern
{
    public class Remote
    {
        #region vars
        Board board;
        #endregion
        #region constructor
        public Remote(Board _board)
        {
            board = _board;
        }
        #endregion
        //runs the command action and passes though the infomaiton needed
        public void Invoke(ICommand cmd)
        {
            cmd.Execute(board);
        }
        //oposite of invoke
        public void Undo(ICommand cmd)
        {
            cmd.Reverse(board);
        }
    }
}
