﻿using System.Collections.Generic;
using System.Linq;
using testing_grid.Classes.UI;
using testing_grid.xaml.Diagram;
using testing_grid.xaml.Diagram.Classes;

namespace testing_grid.Classes.command_pattern
{
    /// <summary>
    /// a class made to change the tiles on the board to a preselected hardware icon type
    /// </summary>
    public class Developer_Board : ICommand
    {
        #region var
        Board board;
        eTileType eTile;
        #endregion
        #region consturctor
        public Developer_Board()
        {
            board = new Board(0, 0);
            eTile = eTileType.blank;
        }
        #endregion
        /// <summary>
        /// changes the tile to the type and size of the tile selected. 
        /// </summary>
        /// <param name="_board"></param>
        public void Execute(Board _board)
        {
            //get the vars set up
            board = _board;
            Point2Int _point = board.Current_Tile;
            //if the current tile is not the tile we have selected change it to a the selected option   
            eTile = board.selected_hardware;
            //run add_hardware fucntion
            add_hardware(_point);
            //varify hardware list we have after adding new icon
            recheckHardware();
            //update the source by changing the object stored in the data location we used to pass though data
            //at start of function
            _board = board;
        }
        /// <summary>
        /// takes tile selected and makes it a blank tile
        /// </summary>
        /// <param name="_board"></param>
        public void Reverse(Board _board)
        {
            board = _board;
            Point2Int _point = board.Current_Tile;
            remove_hardware(_point);
            recheckHardware();
            _board = board;

        }
        /// <summary>
        /// goes though the board we have and rechecks each tile and adds hardware tiles to the hardware list
        /// </summary>
        /// <returns>a updated hardware list</returns>
        private List<Point2Int> recheckHardware()
        {
            //make the list of hardware blank
            List<Point2Int> new_hardware_list = new List<Point2Int>();
            //loop thought the entire board
            for (int i = 0; i < board.tiles.Count; i++)
            {
                //if the current tile being looked at is a head tile (to stop mutilple small images being rendered)
                //and the tiles type isn't a blank tile 
                if ((board.tiles[i].tileType != eTileType.blank) && (board.tiles[i].HeadTile == 2))
                {
                    //add current tile to the hardwaree list
                    new_hardware_list.Add(board.tiles[i]);
                }
            }
            //return the new list
            return new_hardware_list;
        }
        /// <summary>
        /// makes the tile passed though into a head tile,
        /// If larger than the standard size creates the group for the hardware icon
        /// </summary>
        /// <param name="tile">the hardware icon we want the tile selected to be set to</param>
        private void add_hardware(Point2Int currentTile)
        {
            {
                //make the tile clicked on hard ware type equal the type selected 
                currentTile.tileType = eTile;
                //name the tile the hardware type it is 
                currentTile.Name = eTile.ToString();
                //resize
                temp_size_manager(currentTile);
                //see if there's more than one of the hardware type selected in the grid now
                int count = 0;

                for (int i = 0; i < board.Hardware.Count; i++)
                {
                    //if so find out how many there now are
                    if (board.Hardware[i].tileType == eTile)
                    {
                        count++;
                        currentTile.Name = currentTile.tileType.ToString() + " " + count;
                        if (currentTile.Name == board.Hardware[i].Name)
                        {
                            count++;
                            currentTile.Name = currentTile.tileType.ToString() + " " + count;
                        }
                    }
                }
                //add the total number of that hardware type to the name to make the tiles name unuiqe.
                currentTile.HeadTile = 2;
                board.Hardware.Add(currentTile);
                //check the size of the hardware
                if (currentTile.size > 1)
                {
                    //loop though a small mini grid and set the name and hardware to the OG tile
                    for (int i = 0; i < currentTile.size; i++)
                    {
                        for (int j = 0; j < currentTile.size; j++)
                        {
                            //standard finding of a tile selected but with the x and y adjusted to be accurate to our mini grids area 
                            Point2Int Group_Tile = new Point2Int(currentTile.X + j, currentTile.Y + i);
                            //get the index for the large canvas grid
                            int index = ((board.Width / Tile_Texture_Manger.Tile_Size) * Group_Tile.Y) + Group_Tile.X;
                            //set the tile made to that tile 
                            Group_Tile = board.tiles[index];
                            //make the hardware tag the same as the head tile
                            Group_Tile.tileType = currentTile.tileType;
                            //test to see if the current group tile is the head tile
                            if (Group_Tile.HeadTile != 2)
                            {
                                //if it isn't the head tile set to a group tile.
                                Group_Tile.HeadTile = 1;
                            }
                            //add the temp object to the head tiles group
                            currentTile.Group.Add(index);
                            //then make the temp objects group list the same as the head tile
                            Group_Tile.Group = currentTile.Group;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// tempary method of settigng size of a hardware icon
        /// </summary>
        /// <param name="mouseTilePos"></param>
        /// <returns></returns>
        private Point2Int temp_size_manager(Point2Int mouseTilePos)
        {
            //resize hardcoded, will be moved to numeric control in future
            switch (eTile)
            {
                case (eTileType.Network_Analyzer):
                    mouseTilePos.size = 2;
                    mouseTilePos.input_side = direction.bottom;
                    mouseTilePos.output_side = direction.bottom;
                    return mouseTilePos;

                case (eTileType.controller):
                    mouseTilePos.size = 2;
                    mouseTilePos.input_side = direction.right;
                    mouseTilePos.output_side = direction.left;
                    return mouseTilePos;

                case (eTileType.Noise_Meter):
                    mouseTilePos.size = 2;
                    mouseTilePos.input_side = direction.right;
                    mouseTilePos.output_side = direction.right;
                    return mouseTilePos;

                case (eTileType.Tuner):
                    mouseTilePos.size = 2;
                    mouseTilePos.output_side = direction.right;
                    mouseTilePos.input_side = direction.left;
                    return mouseTilePos;

                case (eTileType.Rf_Source):
                    mouseTilePos.input_side = direction.top;
                    mouseTilePos.output_side = direction.top;
                    return mouseTilePos;

                default:
                    return mouseTilePos;
            }
        }
        /// <summary>
        /// blanks the icon passed into the fucntion
        /// </summary>
        /// <param name="tile"></param>
        private void remove_hardware(Point2Int tile)
        {
            #region removing single tile
            //if made blank remove the tile from the hardware list
            if (board.Hardware.Contains(tile))
            {
                Point2Int temp_holder = board.Current_Target;
                //for (int i = 0; i < tile.connected_hardware.Count; i++)
                //{
                //    board.Current_Target = board.tiles[tile.connected_hardware[i]];
                //    Remote remote = new Remote(board);
                //    Routing_Command routing_Command = new Routing_Command();
                //    remote.Undo(routing_Command);
                //}
                //board.Current_Target = temp_holder;
                board.Hardware.Remove(tile);
            }
            tile.tileType = eTileType.blank;
            tile.Name = "blank";
            tile.size = 1;
            tile.HeadTile = 0;
            #endregion

            //create a int equal to the number of tiles in a groupcount of the tile that has been passed though
            int loopsize = tile.Group.Count();
            //for loop to go though all the tiles
            for (int i = 0; i < loopsize; i++)
            {
                //get the index number for the tile we are currently looking at so it can be found in the current_diagram tiles list later on after being blanked 
                int tempIndex = tile.Group[i];
                //create a tile object that uses the location of the group tile in the main display
                Point2Int cleaner = board.tiles[tempIndex];
                //set that to be a new tile object
                cleaner = new Point2Int(cleaner.X, cleaner.Y);
            }
            //clear the group off the orignal tile we set to be safe
            tile.Group.Clear();
            tile = new Point2Int(tile.X, tile.Y);

        }
    }
}
