﻿using System;
using System.Windows.Forms;
using System.Windows.Shapes;
using testing_grid.xaml.Diagram;
using testing_grid.xaml.Diagram.Classes;

namespace testing_grid.Classes.UI
{
    class Routing_Command : ICommand
    {
        #region var
        Board board;
        Point2Int target;
        Point2Int start;
        bool hit;
        #endregion
        /// <summary>
        /// Creates a line and stores it with the other routes
        /// in a list of lines
        /// </summary>
        /// <param name="_board"></param>
        public void Execute(Board _board)
        {
            #region var setup
            board = _board;
            start = board.Current_Tile;
            target = board.Current_Target;
            #endregion
            if (validate_target() && (start.tileType != eTileType.blank))
            {

                Routeing_Configuration _Configuration = new Routeing_Configuration(start.ports_Num, start);
                Line line = _Configuration.Grenerate_Line_Points(target);
                board.Routes.Add(line);
                //find the targets index 
                int index = (target.Y * (board.Width/Tile_Texture_Manger.Tile_Size)) + target.X;
                //add the target to the connected list on the starting tile
                start.connected_hardware.Add(index);
                _board = board;
            }
            else
            {
                MessageBox.Show("please make sure you have selected a target and have choicen a vaild starting point for routing");
                return;
            }
        }
        /// <summary>
        /// Opposite of the above fucntion removes the route from 
        /// two hardware icons selected if the route is there 
        /// </summary>
        /// <param name="_board"></param>
        public void Reverse(Board _board)
        {
            #region var setup
            board = _board;
            start = board.Current_Tile;
            target = board.Current_Target;
            hit = false;
            #endregion

            if (validate_target() && (start.tileType != eTileType.blank))
            {
                //create a line that uses the 
                Routeing_Configuration _Configuration = new Routeing_Configuration(start.ports_Num, start);
                Line line = _Configuration.Grenerate_Line_Points(target);
                line = Line_Comparison(line);

                if(!hit)
                {
                    //flips the start and the target
                    _Configuration = new Routeing_Configuration(target.ports_Num, target);
                    line = _Configuration.Grenerate_Line_Points(start);
                    line = Line_Comparison(line);
                }
                if (!hit)
                {
                    //if the hardware has no route between them 
                    MessageBox.Show("error route doesn't exsist");
                    return;
                }
                else
                {
                    //remove the line
                    board.Routes.Remove(line);
                }
                _board = board;
            }

            
        }
        /// <summary>
        /// makes sure we have a valid target hardware to route to
        /// </summary>
        private bool validate_target()
        {
            bool result = false;
            {
                if (board.ComboBox_Selected_Value == "")
                {
                    return result;
                }
                for (int i = 0; i < board.Hardware.Count; i++)
                {
                    //when we find it make the target into it
                    if (board.Hardware[i].Name == board.ComboBox_Selected_Value)
                    {
                        target = board.Hardware[i];
                        result = true;
                    }
                }
            }
            return result;
        }

        /// <summary>
        ///  compairs all the routes saved again route passed though to the function
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
            private Line Line_Comparison(Line line)
        {
            for (int i = 0; i < board.Routes.Count; i++)
            {
                string Compairable_Line = line.X1.ToString() + line.Y1.ToString() + line.X2.ToString() + line.Y2.ToString();
                string Temp_Test_Line = board.Routes[i].X1.ToString() + board.Routes[i].Y1.ToString() + board.Routes[i].X2.ToString() + board.Routes[i].Y2.ToString();

                if (Temp_Test_Line == Compairable_Line)
                {
                    hit = true;
                     return board.Routes[i];

                }
            }
            hit = false;
            return line;
        }
    }
}
