﻿using System.Collections.Generic;
using System.Xml.Serialization;
using testing_grid.xaml.Diagram;

namespace testing_grid.Classes
{
    /// <summary>
    /// simple class to store key values of the diagarm
    /// </summary>
    [XmlRoot("saveClass")]
    [XmlInclude(typeof(Point2Int))]
    [XmlInclude(typeof(eTileType))]
    //[XmlInclude(typeof(Line))]
    public class saveClass
    {
        public List<Point2Int> HardwareList;
        public List<Point2Int> _Tiles;
        public List<string> Route_Info;
    }
}

