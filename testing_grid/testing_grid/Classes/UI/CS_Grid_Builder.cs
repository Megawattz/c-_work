﻿using System.Collections.Generic;
using testing_grid.xaml.Diagram;

namespace testing_grid.xaml.Diagram
{
    /// <summary>
    /// used to identify whart type of tile tile we are dealing with
    /// </summary>
    public enum eTileType
    {
        Noise_Meter,
        DC_Power,
        Network_Analyzer,
        Rf_Source,
        Switch,
        controller,
        Dut,
        Tuner,
        Bias_Tee,
        //not selectable
        blank
    };

    /// <summary>
    /// class that deals with the grid itself
    /// </summary>
    class CS_Grid_Builder
    {
        public int height;
        public int length;

        /// <summary>
        /// contructor that creates a blank grid
        /// </summary>
        public CS_Grid_Builder()
        {
            height = 0;
            length = 0;
        }

    }


    /// <summary>
    /// deals with positioning of elements within the grid
    /// </summary>
    public class Point2Int
    {

        //for the x postion of the x axis 
        public int X { get; set; }
        //the elements y postion on the y axis as an int 
        public int Y { get; set; }

        //to show the hardware the object has connected to it 
        public List<int> connected_hardware { get; set; }

        //key field for the hardware
        public string Name;

        //emun used to identify hardware group
        public eTileType tileType;

        //for if image takes up a group of tiles
        public List<int> Group;

        //says how many times bigger than one tile this tiles hardware is.
        public int size;

        /// <summary>
        /// these two enums are used to decide which direction a route takes though the object
        /// </summary>
        public direction input_side;
        public direction output_side;

        //used for the number of connections a peice of hardware has
        public int ports_Num;

        /// <summary>
        /// used to indentify the lead tile when a group is linked together
        /// </summary>
        public int HeadTile;

        /// <summary>
        /// tag for making a hardware object removable from the userside.
        /// </summary>
        bool editable;

        /// <summary>
        ///constructor that takes no aurguements in and creates a blank point of 0,0 with no name
        /// </summary>
        public Point2Int()
        {
            X = 0;
            Y = 0;
            connected_hardware = new List<int>();
            Name = " ";
            tileType = eTileType.blank;
            Group = new List<int>();
            size = 32;
            input_side = direction.left;
            output_side = direction.right;
            ports_Num = 1;
            HeadTile = 0;
            editable = false;
        }

        /// <summary>
        /// constructor that takes 2 ints and sets them as coordinates and a name for the hardware(x, y)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point2Int(int x, int y)
        {
            X = x;
            Y = y;
            connected_hardware = new List<int>();
            Name = "blank";
            tileType = eTileType.blank;
            Group = new List<int>();
            size = 1;
            input_side = direction.left;
            output_side = direction.right;
            ports_Num = 0;
            HeadTile = 0;
            editable = false;
        }

    }
}

class Tile_Utils
{
    //***********************************************************************//
    // This function can be used to compute a tile position from a given     //
    // pixel position.                                                       //
    //***********************************************************************//
    public static Point2Int GetTileCoordinatesFromPixel(Point2Int PixelPosition)
    {
        Point2Int ret = new Point2Int();
        ret.X = PixelPosition.X / Tile_Texture_Manger.Tile_Size;
        ret.Y = PixelPosition.Y / Tile_Texture_Manger.Tile_Size;
        return ret;
    }


    //***********************************************************************//
    // This function can be used to compute a pixel position from a given    //
    // tile position.                                                        //
    //***********************************************************************//
    public static Point2Int GetPixelFromTileCoordinates(Point2Int TileCoordinate)
    {
        Point2Int ret = new Point2Int();
        ret.X = TileCoordinate.X * Tile_Texture_Manger.Tile_Size;
        ret.Y = TileCoordinate.Y * Tile_Texture_Manger.Tile_Size;
        return ret;
    }
}

