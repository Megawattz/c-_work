﻿using System.Collections.Generic;
using System.Windows.Shapes;

namespace testing_grid.xaml.Diagram.Classes
{
    public class Board
    {
        #region var
        public int Height;
        public int Width;
        public List<Point2Int> Hardware;
        public List<Point2Int> tiles { get; set; } = new List<Point2Int>();
        public List<Line> Routes;
        public List<string> Route_Info;
        public Point2Int Current_Tile;
        public Point2Int Current_Target;
        public eTileType selected_hardware;
        public string ComboBox_Selected_Value;
        #endregion
        #region constructor
        /// <summary>
        /// creates a blank board with blank storage variables
        /// </summary>
        /// <param name="H"></param>
        /// <param name="W"></param>
        public Board(int H, int W)
        {
            Height = H;
            Width = W;
            Hardware = new List<Point2Int>();
            Routes = new List<Line>();
            Route_Info = new List<string>();
            Current_Tile = new Point2Int();
            Current_Target = new Point2Int();
            selected_hardware = eTileType.blank;
            ComboBox_Selected_Value = "";

        }
        #endregion
        /// <summary>
        /// This function resizes the board to the given size. 
        /// Currently this can only be used to initialise an
        /// empty board.
        /// </summary>
        public void Resize(int sizeX, int sizeY, bool newboard = false)
        {
            if (sizeX == Width && sizeY == Height)
            {
                return;
            }


            if (newboard)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    for (int x = 0; x < sizeX; x++)
                    {
                        tiles.Add(new Point2Int(x, y));
                    }
                }


                Width = sizeX;
                Height = sizeY;
            }
        }
    }
}
