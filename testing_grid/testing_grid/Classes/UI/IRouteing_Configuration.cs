﻿using System.Windows.Shapes;
using testing_grid.xaml.Diagram;

namespace testing_grid.Classes
{
    interface IRouteing_Configuration
    {
        Line CreateLine(int x1, int y1, int x2, int y2, bool AddLine = false);
        Line Grenerate_Line_Points(Point2Int Current_Target);
        Point2Int convertXY(direction Facing, Point2Int holder);
    }
}