﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace testing_grid.xaml.Diagram
{
    /// <summary>
    /// stores hardware textures and deals with the size of the tiles in the diagram
    /// </summary>
    ///
    class Tile_Texture_Manger
    {
        #region vars
        public List<string> bitmaps;

        //controls the size of the board tile sizes
        public const int Tile_Size = 1;

        //bitmap images ready for modifiying for the grid
        public BitmapImage RF_Source { get; set; }
        public BitmapImage Network_Analyzer { get; set; }
        public BitmapImage Blank_Icon { get; set; }
        public BitmapImage Noise_Meter { get; set; }
        public BitmapImage DC_Power { get; set; }
        public BitmapImage Dut { get; set; }
        public BitmapImage Tuner { get; set; }
        public BitmapImage Controller { get; set; }
        public BitmapImage Bias_Tee { get; set; }
        public BitmapImage Switch { get; set; }

        #endregion
        /// <summary>
        /// contructor
        /// </summary>
        public Tile_Texture_Manger()
        {
            bitmaps = new List<string>();
            bitmaps.Add("RF_Source");
            bitmaps.Add("Network_Analyzer");
            bitmaps.Add("Noise_Meter");
            bitmaps.Add("DC_Power");
            bitmaps.Add("Dut");
            bitmaps.Add("Tuner");
            bitmaps.Add("Controller");
            bitmaps.Add("Bias_Tee");
            bitmaps.Add("Switch");
        }
    }


    static class Paser
    {

        /// <summary>
        /// This function imports the game textures into a CGameTexture j
        /// object from the given directory. All textures should be present.
        /// </summary>
        /// <param name="directory">The directory where the textures are stored</param>
        /// <returns>An object storing the loading texteures.</returns>
        public static Tile_Texture_Manger ImportTextures(Tile_Texture_Manger current, string directory)
        {
            Tile_Texture_Manger ret = current;
            ret.Blank_Icon = new BitmapImage(new System.Uri(directory + "\\blankImage.jpg"));
            ret.Bias_Tee = new BitmapImage(new System.Uri(directory + "\\Bias_Tee.jpg"));
            ret.Controller = new BitmapImage(new System.Uri(directory + "\\Controller.jpg"));
            ret.DC_Power = new BitmapImage(new System.Uri(directory + "\\DC_Power.jpg"));
            ret.Dut = new BitmapImage(new System.Uri(directory + "\\Dut.jpg"));
            ret.Network_Analyzer = new BitmapImage(new System.Uri(directory + "\\Network_Analyser.jpg"));
            ret.Noise_Meter = new BitmapImage(new System.Uri(directory + "\\Noise_Meter.jpg"));
            ret.RF_Source = new BitmapImage(new System.Uri(directory + "\\RF_Source.jpg"));
            ret.Switch = new BitmapImage(new System.Uri(directory + "\\Switch.jpg"));
            ret.Tuner = new BitmapImage(new System.Uri(directory + "\\Tuner.jpg"));

            return ret;
        }
    }
}
