﻿using System;
using System.Windows.Forms;

namespace impedance_test
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Inpedance_test_form());
            }
                   catch (Exception ex) 
            {
                string msg = "Application Failed To Start\n\n";                    // + ex.Message;
                string stackTrace = "Stack Trace\n";

                msg += ex.Message + "\n";

                while (ex.InnerException != null)
                {
                    msg += ex.Message + "\n";
                    stackTrace += ex.StackTrace;
                    ex = ex.InnerException;
                } 

                System.IO.File.WriteAllText("ImpedanceError.txt", msg + "\n\n");
                System.IO.File.AppendAllText("ImpedanceError.txt", stackTrace);

                MessageBox.Show(msg, "Application Failed To Start", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
