﻿namespace impedance_test
{
    partial class Inpedance_test_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inpedance_test_form));
            this.but_connect = new System.Windows.Forms.Button();
            this.but_Disconnect = new System.Windows.Forms.Button();
            this.But_start = new System.Windows.Forms.Button();
            this.but_cease = new System.Windows.Forms.Button();
            this.output_box = new System.Windows.Forms.ListBox();
            this.messure_timer = new System.Windows.Forms.Timer(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.but_clear_log = new System.Windows.Forms.Button();
            this.But_Reconfigure = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // but_connect
            // 
            this.but_connect.Location = new System.Drawing.Point(47, 39);
            this.but_connect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.but_connect.Name = "but_connect";
            this.but_connect.Size = new System.Drawing.Size(87, 28);
            this.but_connect.TabIndex = 0;
            this.but_connect.Text = "Connect";
            this.but_connect.UseVisualStyleBackColor = true;
            this.but_connect.Click += new System.EventHandler(this.button_hit);
            // 
            // but_Disconnect
            // 
            this.but_Disconnect.Location = new System.Drawing.Point(140, 39);
            this.but_Disconnect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.but_Disconnect.Name = "but_Disconnect";
            this.but_Disconnect.Size = new System.Drawing.Size(87, 28);
            this.but_Disconnect.TabIndex = 0;
            this.but_Disconnect.Text = "Disconnect";
            this.but_Disconnect.UseVisualStyleBackColor = true;
            this.but_Disconnect.Click += new System.EventHandler(this.button_hit);
            // 
            // But_start
            // 
            this.But_start.Location = new System.Drawing.Point(700, 39);
            this.But_start.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.But_start.Name = "But_start";
            this.But_start.Size = new System.Drawing.Size(87, 28);
            this.But_start.TabIndex = 0;
            this.But_start.Text = "Start_Test";
            this.But_start.UseVisualStyleBackColor = true;
            this.But_start.Click += new System.EventHandler(this.button_hit);
            // 
            // but_cease
            // 
            this.but_cease.Location = new System.Drawing.Point(794, 39);
            this.but_cease.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.but_cease.Name = "but_cease";
            this.but_cease.Size = new System.Drawing.Size(87, 28);
            this.but_cease.TabIndex = 0;
            this.but_cease.Text = "Stop_Test";
            this.but_cease.UseVisualStyleBackColor = true;
            this.but_cease.MouseCaptureChanged += new System.EventHandler(this.button_hit);
            // 
            // output_box
            // 
            this.output_box.FormattingEnabled = true;
            this.output_box.ItemHeight = 16;
            this.output_box.Location = new System.Drawing.Point(14, 170);
            this.output_box.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.output_box.Name = "output_box";
            this.output_box.Size = new System.Drawing.Size(905, 164);
            this.output_box.TabIndex = 1;
            // 
            // messure_timer
            // 
            this.messure_timer.Interval = 60000;
            this.messure_timer.Tick += new System.EventHandler(this.messure_timer_Tick);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(700, 130);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(182, 28);
            this.progressBar1.TabIndex = 2;
            // 
            // but_clear_log
            // 
            this.but_clear_log.Location = new System.Drawing.Point(794, 75);
            this.but_clear_log.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.but_clear_log.Name = "but_clear_log";
            this.but_clear_log.Size = new System.Drawing.Size(87, 28);
            this.but_clear_log.TabIndex = 0;
            this.but_clear_log.Text = "clear log";
            this.but_clear_log.UseVisualStyleBackColor = true;
            this.but_clear_log.Click += new System.EventHandler(this.button_hit);
            // 
            // But_Reconfigure
            // 
            this.But_Reconfigure.Location = new System.Drawing.Point(701, 75);
            this.But_Reconfigure.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.But_Reconfigure.Name = "But_Reconfigure";
            this.But_Reconfigure.Size = new System.Drawing.Size(87, 28);
            this.But_Reconfigure.TabIndex = 0;
            this.But_Reconfigure.Text = "Reconfigure";
            this.But_Reconfigure.UseVisualStyleBackColor = true;
            this.But_Reconfigure.Click += new System.EventHandler(this.button_hit);
            // 
            // Inpedance_test_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 350);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.output_box);
            this.Controls.Add(this.but_cease);
            this.Controls.Add(this.But_Reconfigure);
            this.Controls.Add(this.but_clear_log);
            this.Controls.Add(this.But_start);
            this.Controls.Add(this.but_Disconnect);
            this.Controls.Add(this.but_connect);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Inpedance_test_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Impedance test form";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button but_connect;
        private System.Windows.Forms.Button but_Disconnect;
        private System.Windows.Forms.Button But_start;
        private System.Windows.Forms.Button but_cease;
        private System.Windows.Forms.ListBox output_box;
        private System.Windows.Forms.Timer messure_timer;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button but_clear_log;
        private System.Windows.Forms.Button But_Reconfigure;
    }
}

