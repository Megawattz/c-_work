﻿using ConsoleRemoteAccessLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Numerics;
using System.Windows.Forms;

namespace impedance_test
{
    public partial class Inpedance_test_form : Form
    {
        #region var
        bool debug;
        List<Complex> Complex_list;
        DateTime expired_date_time;
        string lineout;
        string path;
        StreamWriter sw;
        #endregion
        public Inpedance_test_form()
        {
            InitializeComponent();
            setup();
        }

        /// <summary>
        /// creates a output message for the user to see whats happening
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="func"></param>
        /// <param name="level"></param>
        private void output(string msg, string func, int level)
        {
            string spacer = "  ";
            string dateTime = System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");

            string spacedfunc = func + new string(' ', 25);
            spacedfunc = spacedfunc.Substring(0, 24);

            string indent = new string(' ', level * 4);

            output_box.Items.Add(dateTime + spacer + spacedfunc + indent + msg);
            output_box.TopIndex = output_box.Items.Count - 1;
        }

        private void Init()
        {
            output_box.Font = new Font(FontFamily.GenericMonospace, output_box.Font.Size);
            string mode = NILabviewClient.consoleGetMode();

        }

        private string GetRemoteResult()
        {
            string result = "";
            if (NILabviewClient.IsException() != -1)
            {
                result = ((NILabviewClient.IsException() == 0) ? "Success" : "Failed");
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        void test_connection(int level)
        {
            string funcName = "CheckCounter()";
            for (int i = 0; i < 10; i++)
            {
                int c = NILabviewClient.CheckCounter();
                output("counter = " + c.ToString(), funcName, level);
            }
        }

        /// <summary>
        /// returns a complex number from two doubles passed though to it
        /// </summary>
        /// <param name="real">real part of the complex</param>
        /// <param name="imagenary">the imagenary part of the new complex</param>
        /// <returns></returns>
        Complex GetComplex(double real, double imagenary)
        {
            Complex complexDouble = new Complex(real, imagenary);
            return complexDouble;
        }

        /// <summary>
        /// takes in values from a file dialog and converts it to a complex number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Get_Impedance()
        {
            #region local values
            string functionName = "readin_values";
            int level = 3;
            List<double> tempNumbers = new List<double>();
            Complex_list = new List<Complex>();
            string error = "";

            #endregion
            //open the file we wish to read
            OpenFileDialog fileDialog = new OpenFileDialog();

            //if we get a file back
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    //take its path as a string and read it into a reader
                    string filepath = fileDialog.FileName;
                    string[] lines = System.IO.File.ReadAllLines(filepath);

                    if (lines.Length != 0)
                    {
                        //go though all lines read by the reader
                        for (int i = 1; i < lines.Length; i++)
                        {
                            //split the line inti individual numbers

                            string[] tmp = lines[i].Split(',');
                            double r = double.Parse(tmp[0]);
                            double im = double.Parse(tmp[1]);
                            Complex_list.Add(GetComplex(r, im));
                        }
                    }
                    else
                    {
                        throw new CustomExcpetion(string.Format("cannot find any double values in file", filepath));
                    }
                }
                catch (Exception d)
                {
                    error = d.Message;
                }

                if (Complex_list.Count != 0)
                {
                    output("sucess number of complex numebers found : " + Complex_list.Count, functionName, level);
                }
                else
                {
                    output("failure " + error, functionName, level);
                }

            }
        }

        private Complex Measure(int i)
        {
            double Gr = 0;
            double Gi = 0;
            if (Complex_list.Count == 0)
            {
                NILabviewClient.measTriggerSingle();
                NILabviewClient.measGetGamma(1, 0, out Gr, out Gi);
                return new Complex(Gr, Gi);
            }
            else
            {
                Complex SetLoad = Complex_list[i];
                NILabviewClient.tunerSetGamma(1, SetLoad.Real, SetLoad.Imaginary);
                NILabviewClient.measTriggerSingle();
                NILabviewClient.measGetGamma(1, 0, out Gr, out Gi);
                return new Complex(Gr, Gi);
            }
        }

        private double ErrorFunction(Complex Set, Complex Measured)
        {

            return 20 * Math.Log10((Set - Measured).Magnitude);
        }

        /// <summary>
        /// every time goes off reduces number by 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void messure_timer_Tick(object sender, EventArgs e)
        {
            string functionName = "timer";
            int level = 4;
            string time = System.DateTime.Now.ToShortTimeString();
            if (timeExpired())
            {
                messure_timer.Stop();
                output("Timer ended" + DateTime.Now, functionName, level);
            }
            else
            {
                //to store the outputs impedance 
                List<Complex> outputs = new List<Complex>();

                //stores the differance between the input and output impedance
                List<double> error = new List<double>();

                //seperator used to split output line into cells in output document 
                string sep = ",";

                Exception filler_error = null;

                //makes sure there is a filled list to use for testing 
                if (Complex_list.Count != 0)
                #region with complex read in
                {
                    for (int i = 0; i < Complex_list.Count; i++)
                    {
                        if (!debug)
                        {
                            //calcuate and store the output of the mesurement
                            outputs.Add(Measure(i));
                            //calcuate and store the error
                            error.Add(ErrorFunction(Complex_list[i], outputs[i]));
                        }
                    }
                    try
                    {

                        using (sw = File.AppendText(path))
                        {
                            sw.WriteLine("");
                            for (int i = 0; i < Complex_list.Count; i++)
                            {
                                lineout = i + sep + Complex_list[i].Real.ToString() + sep + Complex_list[i].Imaginary.ToString() + sep + outputs[i].Real.ToString() + sep + outputs[i].Imaginary.ToString() + sep + error[i] + sep + time;
                                sw.WriteLine(lineout);
                            }
                        }
                    }
                    catch (Exception X)
                    {

                        output(X.Message.ToString(), functionName, level);
                        filler_error = X;
                    }
                    #endregion
                }
                if (filler_error == null)
                {
                    output("processed points once", functionName, level);
                }
                else
                {
                    messure_timer.Stop();
                    output("testing stopped due to: " + filler_error.Message.ToString(), functionName, level);
                }
            }
        }

        private bool timeExpired()
        {
            return (DateTime.Compare(DateTime.Now, expired_date_time) > 0);
        }

        private void setup()
        {
            //should be set to false before running 
            debug = false;
            Complex_list = new List<Complex>();

            //make sure read in txt file for input
            MessageBox.Show("Please select file with input inpedance in");
            Get_Impedance();

            //value to change how long code runs for  
            expired_date_time = DateTime.Now.AddHours(12);

            //deals with output of the software
            MessageBox.Show("Please choose which file to output data to.");

            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.DefaultExt = "csv";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                path = fileDialog.FileName;
            }
        }

        private void button_hit(object sender, EventArgs eventArgs)
        {
            Button button = (Button)sender;

            switch (button.Name)
            {
                case "but_connect":
                    #region connect Button
                    string funcName = "CreateConnection()";
                    int level = 1;

                    try
                    {
                        //get the address to connect to
                        NILabviewClient.CreateConnection();
                        output("Connection to Server has been established ...", funcName, level);
                        //make sure the connection is active
                        funcName = "remoteGetStatus()";
                        NILabviewClient.remoteActivate();
                        output("Connection to Server has been activated ...", funcName, level);


                        output("Conneted = " + NILabviewClient.remoteGetStatus(), funcName, level);

                        Init();
                    }
                    catch (Exception e)
                    {
                        output(e.Message.ToString(), funcName, level);
                    }

                    break;
                #endregion

                case "but_Disconnect":
                    #region disconnect button
                    funcName = "disconnect()";
                    level = 1;
                    output("Timer has been stopped", funcName, level);
                    NILabviewClient.remoteDeactivate();
                    output("Connection to Server has been deactivated", funcName, level);

                    funcName = "remoteGetStatus()";
                    //led1.Value = NILabviewClient.remoteGetStatus();
                    //SetVisualStatus(lbl_VisStatus, NILabviewClient.remoteGetStatus());
                    output("Conneted = " + NILabviewClient.remoteGetStatus(), funcName, level);
                    if (NILabviewClient.IsException() == 1)
                    {
                        output("Not started due to existing exception- " + NILabviewClient.GetExceptionMessage(), funcName, level);
                        NILabviewClient.ResetException();   // Reset error, now reported. 
                        output(GetRemoteResult(), "ResetException()", 2);
                    }
                    break;
                #endregion

                case "But_start":
                    #region start_test_button
                    funcName = "start_button()";
                    level = 3;
                    bool pass = true;
                    if (NILabviewClient.IsException() == 0)
                    {
                        try
                        {
                            //make first element of output line 
                            lineout = "index, input real, input imag , outputs real, out imag , error, time";
                            System.IO.File.WriteAllText(path, lineout);

                            //start the timer
                            messure_timer.Start();
                        }
                        catch (Exception error1)
                        {
                            //if we have an errror in the startup
                            output(error1.Message, funcName, level);
                            pass = false;
                        }
                        if (pass)
                        {
                            output("sucess ", funcName, level);
                        }
                    }
                    break;
                #endregion

                case "but_cease":
                    #region stop Test
                    funcName = "stop timer";
                    level = 3;
                    messure_timer.Stop();
                    output("Timer has been stopped", funcName, level);
                    break;
                #endregion

                case "but_clear_log":
                    #region clear log button
                    output_box.Items.Clear();
                    break;
                #endregion

                case "But_Reconfigure":
                    #region But_Reconfigure
                    funcName = "Reconfigure()";
                    level = 1;
                    setup();
                    output("path reset to " + path, funcName, level);
                    break;
                #endregion

                default:
                    #region error button
                    funcName = "button_error()";
                    level = 1;
                    output("button doesn't exsist in button list ", funcName, level);
                    break;
                    #endregion
            }
        }
    }




}

public class CustomExcpetion : System.Exception
{
    public CustomExcpetion(string message)
       : base(message) { }
}


