#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "4DF226E3C5CF826BF46269146B8D7EB145CF38022931966BF937E7843F69D731"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace corner
{


    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector
    {


#line 10 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid main_grid;

#line default
#line hidden


#line 12 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ColumnDefinition col_1;

#line default
#line hidden


#line 13 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ColumnDefinition col_2;

#line default
#line hidden


#line 20 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas testBed;

#line default
#line hidden


#line 58 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Line_Label;

#line default
#line hidden


#line 67 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Route_List;

#line default
#line hidden


#line 76 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Elispe_Box;

#line default
#line hidden


#line 92 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider Thickness;

#line default
#line hidden


#line 98 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Thickenss_Text;

#line default
#line hidden


#line 106 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel Add_Pannel;

#line default
#line hidden


#line 108 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add_Button;

#line default
#line hidden


#line 119 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add_Line_Point;

#line default
#line hidden

        private bool _contentLoaded;

        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
            if (_contentLoaded)
            {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/corner;component/mainwindow.xaml", System.UriKind.Relative);

#line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);

#line default
#line hidden
        }

        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
        {
            switch (connectionId)
            {
                case 1:
                    this.main_grid = ((System.Windows.Controls.Grid)(target));
                    return;
                case 2:
                    this.col_1 = ((System.Windows.Controls.ColumnDefinition)(target));
                    return;
                case 3:
                    this.col_2 = ((System.Windows.Controls.ColumnDefinition)(target));
                    return;
                case 4:
                    this.testBed = ((System.Windows.Controls.Canvas)(target));

#line 19 "..\..\MainWindow.xaml"
                    this.testBed.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.click);

#line default
#line hidden
                    return;
                case 5:
                    this.Line_Label = ((System.Windows.Controls.TextBlock)(target));
                    return;
                case 6:
                    this.Route_List = ((System.Windows.Controls.ComboBox)(target));

#line 68 "..\..\MainWindow.xaml"
                    this.Route_List.DropDownOpened += new System.EventHandler(this.Route_List_DropDownOpened);

#line default
#line hidden

#line 69 "..\..\MainWindow.xaml"
                    this.Route_List.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Route_List_SelectionChanged);

#line default
#line hidden
                    return;
                case 7:
                    this.Elispe_Box = ((System.Windows.Controls.ComboBox)(target));

#line 79 "..\..\MainWindow.xaml"
                    this.Elispe_Box.DropDownOpened += new System.EventHandler(this.Elispe_Box_DropDownOpened);

#line default
#line hidden

#line 80 "..\..\MainWindow.xaml"
                    this.Elispe_Box.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Elispe_Box_SelectionChanged);

#line default
#line hidden
                    return;
                case 8:
                    this.Thickness = ((System.Windows.Controls.Slider)(target));

#line 95 "..\..\MainWindow.xaml"
                    this.Thickness.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.Thickness_ValueChanged);

#line default
#line hidden
                    return;
                case 9:
                    this.Thickenss_Text = ((System.Windows.Controls.TextBlock)(target));
                    return;
                case 10:
                    this.Add_Pannel = ((System.Windows.Controls.StackPanel)(target));
                    return;
                case 11:
                    this.Add_Button = ((System.Windows.Controls.Button)(target));

#line 109 "..\..\MainWindow.xaml"
                    this.Add_Button.Click += new System.Windows.RoutedEventHandler(this.Add_Button_Click);

#line default
#line hidden
                    return;
                case 12:
                    this.Add_Line_Point = ((System.Windows.Controls.Button)(target));

#line 124 "..\..\MainWindow.xaml"
                    this.Add_Line_Point.Click += new System.Windows.RoutedEventHandler(this.Add_Line_Point_Click);

#line default
#line hidden
                    return;
            }
            this._contentLoaded = true;
        }

        internal System.Windows.Controls.StackPanel Start_End_Setting;
        internal System.Windows.Controls.TextBox Start_X_Start;
        internal System.Windows.Controls.TextBox Start_X_End;
        internal System.Windows.Controls.TextBox End_X_End;
        internal System.Windows.Controls.TextBox End_X_Start;
    }
}

