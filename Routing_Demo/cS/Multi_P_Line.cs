﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;

namespace corner.cS
{
    class Multi_P_Line
    {
        #region vars
        public Polyline polyline;
        public List<Point> Editiable_Points;
        public Point start;
        public Point end;
        #endregion
        #region constructor
        public Multi_P_Line(int Number_Of_Points, Point Start, Point End)
        {
            Editiable_Points = new List<Point>();
            start = Start;
            end = End;
            createPoints(Number_Of_Points);
            polyline = new Polyline();
        }
        #endregion
        /// <summary>
        /// creates the list of points
        /// </summary>
        /// <param name="count"></param>
        private void createPoints(int count)
        {
            Editiable_Points.Add(start);
            for (int itr = 0; itr < count; itr++)
            {
                Editiable_Points.Add(new Point(0, 0));
            }
            Editiable_Points.Add(end);
        }
    }



}
